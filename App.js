import React from 'react';

import {Provider} from 'react-redux';

import Index from './src/router/Index';
import store from './src/store/store';
import {
  // MD3LightTheme as DefaultTheme,
  Provider as PaperProvider,
} from 'react-native-paper';
// import {LogBox} from 'react-native';

const App = () => {
  // const theme = {
  //   ...DefaultTheme,
  //   colors: {
  //     ...DefaultTheme.colors,
  //     primary: 'tomato',
  //     secondary: 'yellow',
  //   },
  // };
  // LogBox.ignoreLogs(['Invalid prop textStyle of type array supplied to Cell']);
  return (
    <PaperProvider>
      <Provider store={store}>
        <Index />
      </Provider>
    </PaperProvider>
  );
};

export default App;
