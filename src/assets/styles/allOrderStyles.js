import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e4e4e4',
  },
  tabArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: hp('1%'),
    marginHorizontal: wp('5%'),
  },
  tabButton: {
    paddingVertical: hp('2%'),
    backgroundColor: '#fff',
    width: wp('45%'),
  },
  tabButtonText: {
    textAlign: 'center',
    color: '#000',
  },
  activeTabButton: {
    borderBottomColor: '#d2181b',
    borderBottomWidth: 2,
  },
  activeTabButtonText: {
    color: '#d2181b',
  },
  continueButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1%'),
    paddingHorizontal: wp('3%'),
    borderRadius: 8,
  },
  continueText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
});

export default styles;
