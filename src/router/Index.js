import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Home,
  Login,
  Register,
  Loading,
  Driver,
  MyBookings,
  BusinessProfile,
  Order,
  Settings,
  Printer,
  Terminal,
  ForgotPassword,
  ResetPassword,
  Notifications,
  SalesReport,
  MyOrder,
  HouseOrder,
} from './all';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import OrderNotification from '../services/order-notification/OrderNotification';
import BookingNotification from '../services/BookingNotification';
import TermsAndConditions from '../screens/home/TermsAndConditions';
import InHouseOrder from '../screens/in-house/InHouseOrder';
import RestaurantDetails from '../screens/in-house/restaurant-details/RestaurantDetails';
import AddToCart from '../screens/in-house/add-to-cart/AddToCart';
import InHouseOrderDetails from '../utilities/in-house-order/order-details/OrderDetails';
import MyOrderDetails from '../utilities/my-order/order-details/OrderDetails';
import DineInOrderNotification from '../services/DineInOrderNotification';
import InHouseDineInPayment from '../screens/in-house/dine-in-payment/InHouseDineInPayment';
import InHouseDineInCardPayment from '../screens/in-house/dine-in-payment/InHouseDineInCardPayment';
import InHouseCardPayment from '../screens/in-house/InHouseCardPayment';
import InHouseSumupPayment from '../screens/in-house/InHouseSumupPayment';
import Payment from '../screens/in-house/payment/Payment';
import Invoice from '../screens/invoice/Invoice';
// import Terminal from '../screens/terminal/TerminalScreen';

const Tab = createMaterialBottomTabNavigator();
import {Icon} from 'native-base';

// import SoundPlayerComponent from "../screens/home/SoundPlayer";

function SecondStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainScreen"
        options={defaultStackConfig}
        component={Home}
      />
      <Stack.Screen
        name="HouseOrder"
        options={{
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          title: 'House Order',
          headerLeft: null,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        component={HouseOrder}
      />
    </Stack.Navigator>
  );
}

function HomeTabs() {
  return (
    <Tab.Navigator
      activeColor="#adadad"
      inactiveColor="#adadad"
      barStyle={{backgroundColor: '#f0eded', paddingBottom: 8, height: 0}}>
      <Tab.Screen
        name=" "
        options={{
          // tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <Icon
              // type="Ionicons"
              // name="home-outline"
              style={{fontSize: 24, color: color}}
            />
          ),
        }}
        component={SecondStack}
      />
      {/*<Tab.Screen name="Chat"
                        options={{
                            tabBarLabel: "Chat",
                            tabBarIcon : ({color}) => (
                                <Icon type="Ionicons" name="chatbubble-outline"
                                      style={{fontSize: 24, color: color}} />
                            ),
                        }}
                        component={Chat} />*/}
      {/* <Tab.Screen
      // name="Settings"
      // options={{
      //   tabBarLabel: 'Settings',
      //   tabBarIcon: ({color}) => (
      //     <Icon
      //       type="AntDesign"
      //       name="setting"
      //       style={{fontSize: 24, color: color}}
      //     />
      //   ),
      // }}
      // component={Settings}
      /> */}
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();
const defaultStackConfig = {
  headerShown: false,
  gestureEnabled: true,
  gestureDirection: 'horizontal',
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};

export class Index extends Component {
  render() {
    if (!this.props.loggedIn) {
      return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name="Loading"
              options={defaultStackConfig}
              component={Loading}
            />
            <Stack.Screen
              name="Login"
              options={defaultStackConfig}
              component={Login}
            />
            <Stack.Screen
              name="Register"
              options={defaultStackConfig}
              component={Register}
            />
            <Stack.Screen
              name="ForgotPassword"
              options={defaultStackConfig}
              component={ForgotPassword}
            />
            <Stack.Screen
              name="ResetPassword"
              options={defaultStackConfig}
              component={ResetPassword}
            />
          </Stack.Navigator>
        </NavigationContainer>
      );
    } else {
      return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name="HomeScreen"
              options={defaultStackConfig}
              children={HomeTabs}
            />
            {/*<Stack.Screen name="SoundPlayerComponent" options={{
                         title                : "SoundPlayerComponent", gestureEnabled: true,
                         gestureDirection     : "horizontal",
                         cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                         }} component={SoundPlayerComponent} />*/}
            <Stack.Screen
              name="MyOrder"
              options={{
                title: 'My Order',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={MyOrder}
            />
            <Stack.Screen
              name="Order"
              options={{
                title: 'Orders',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Order}
            />
            <Stack.Screen
              name="Printer"
              options={{
                title: 'Orders',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Printer}
            />
            <Stack.Screen
              name="Terminal"
              options={{
                title: 'Orders',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Terminal}
            />
            <Stack.Screen
              name="Notifications"
              options={{
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Notifications}
            />
            <Stack.Screen
              name="Driver"
              options={{
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                title: 'Driver Nearby',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Driver}
            />

            <Stack.Screen
              name="TermsAndConditions"
              options={{
                title: 'Terms & Conditions',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={TermsAndConditions}
            />
            <Stack.Screen
              name="MyBookings"
              options={{
                title: 'My Bookings',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={MyBookings}
            />
            <Stack.Screen
              name="SalesReport"
              options={{
                title: 'Sales Report',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={SalesReport}
            />
            <Stack.Screen
              name="Invoice"
              options={{
                title: 'Invoice',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Invoice}
            />
            <Stack.Screen
              name="OrderNotification"
              options={{
                headerShown: false,
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={OrderNotification}
            />
            <Stack.Screen
              name="DineInOrderNotification"
              options={{
                headerShown: false,
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={DineInOrderNotification}
            />
            <Stack.Screen
              name="BookingNotification"
              options={{
                headerShown: false,
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={BookingNotification}
            />
            <Stack.Screen
              name="InHouseOrder"
              options={{
                title: 'In House',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={InHouseOrder}
            />
            <Stack.Screen
              name="RestaurantDetails"
              options={{
                title: 'Restaurant',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={RestaurantDetails}
            />
            <Stack.Screen
              name="AddToCart"
              options={{
                title: 'Restaurant',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={AddToCart}
            />
            <Stack.Screen
              name="Payment"
              options={{
                title: 'Checkout',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={Payment}
            />
            <Stack.Screen
              name="BusinessProfile"
              options={{
                title: 'Business Profile',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={BusinessProfile}
            />
            <Stack.Screen
              name="InHouseOrderDetails"
              options={{
                title: 'Order Details',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={InHouseOrderDetails}
            />
            <Stack.Screen
              name="MyOrderDetails"
              options={{
                title: 'Order Details',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={MyOrderDetails}
            />
            <Stack.Screen
              name="InHouseDineInPayment"
              options={{
                title: 'Payment Method',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={InHouseDineInPayment}
            />
            <Stack.Screen
              name="InHouseDineInCardPayment"
              options={{
                title: 'Card Payment',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={InHouseDineInCardPayment}
            />
            <Stack.Screen
              name="InHouseCardPayment"
              options={{
                title: 'Card Payment',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={InHouseCardPayment}
            />
            <Stack.Screen
              name="InHouseSumupPayment"
              options={{
                title: 'Sumup Payment',
                gestureEnabled: true,
                gestureDirection: 'horizontal',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
              component={InHouseSumupPayment}
            />
          </Stack.Navigator>
        </NavigationContainer>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    host: state.auth.host,
    loggedIn: state.auth.loggedIn,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
