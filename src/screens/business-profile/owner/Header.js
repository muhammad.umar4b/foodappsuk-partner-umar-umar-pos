import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import Feather from "react-native-vector-icons/Feather";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

const Header = (props) => {
    const {
        updateUser,
        inputEditable,
    } = props;

    const {
        cardHeaderLabel,
        circleIconArea,
        cardHeaderEditText,
    } = styles;

    const {
        cardHeader,
        boxShadow,
        elevation5,
        marginLeft06,
        flexDirectionRow,
        paddingTop05,
        marginRight1,
    } = globalStyles;

    return (
        <View style={cardHeader}>
            <View style={flexDirectionRow}>
                <View style={[circleIconArea, boxShadow, elevation5, marginLeft06]}>
                    <Feather name="user" size={20} color="#D2181B" />
                </View>
                <Text style={cardHeaderLabel}>Update Profile</Text>
            </View>

            <TouchableOpacity style={[flexDirectionRow, paddingTop05]}
                              onPress={() => updateUser()}>
                {!inputEditable &&
                    <MaterialIcons name="edit" size={20} color="#555555" />
                }

                {inputEditable &&
                    <Feather style={marginRight1} name="check-circle" size={20} color="#555555" />
                }

                <Text style={cardHeaderEditText}>
                    {inputEditable ? "Save" : "Edit"}
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default Header;
