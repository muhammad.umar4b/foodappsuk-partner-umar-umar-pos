import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    circleIconArea: {
        backgroundColor: "#fff",
        width: 30,
        height: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
    },
    cardHeaderLabel: {
        color: "#555555",
        fontSize: 18,
        fontWeight: "bold",
        paddingLeft: wp("2%"),
        paddingTop: hp("0.5%"),
    },
    cardHeaderEditText: {
        color: "#555555",
        fontSize: 16,
        fontWeight: "bold",
        paddingLeft: wp("0.3%"),
    },
    inputField: {
        paddingVertical: hp("1%"),
        paddingLeft: wp("4%"),
        borderColor: "#d9d3d3",
        borderWidth: 1,
        borderRadius: 8,
    },
    inputLabel: {
        color: "#555555",
    },
    profileArea: {
        marginHorizontal: wp("5%"),
        marginVertical: hp("3%"),
    },
});

export default styles;
