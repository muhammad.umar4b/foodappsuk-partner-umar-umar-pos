import React from "react";
import {ScrollView, Text, TouchableOpacity, View, StyleSheet, TextInput, Pressable} from "react-native";
import Modal from "react-native-modal";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import globalStyles from "../../../assets/styles/globalStyles";

const timings = [
    "12:30 AM",
    "01:00 AM",
    "01:30 AM",
    "02:00 AM",
    "02:30 AM",
    "03:00 AM",
    "03:30 AM",
    "04:00 AM",
    "04:30 AM",
    "05:00 AM",
    "05:30 AM",
    "06:00 AM",
    "06:30 AM",
    "07:00 AM",
    "07:30 AM",
    "08:00 AM",
    "08:30 AM",
    "09:00 AM",
    "09:30 AM",
    "10:00 AM",
    "10:30 AM",
    "11:00 AM",
    "11:30 AM",
    "12:00 PM",
    "12:30 PM",
    "01:00 PM",
    "01:30 PM",
    "02:00 PM",
    "02:30 PM",
    "03:00 PM",
    "03:30 PM",
    "04:00 PM",
    "04:30 PM",
    "05:00 PM",
    "05:30 PM",
    "06:00 PM",
    "06:30 PM",
    "07:00 PM",
    "07:30 PM",
    "08:00 PM",
    "08:30 PM",
    "09:00 PM",
    "09:30 PM",
    "10:00 PM",
    "10:30 PM",
    "11:00 PM",
    "11:30 PM",
    "12:00 AM"
];

const ClosingTimeModal = (props) => {
    const {
        closingTimeModalVisible,
        setClosingTimeModalVisible,
        state,
        setState,
        inputEditable
    } = props;

    const {
        closingTime,
    } = state;

    const {
        modalView,
        modalBody,
        modalFooter,
        monthArea,
        monthAreaContent,
        monthAreaContentBlock,
        monthAreaContentText,
        monthAreaContentTextActive,
        monthAreaHeaderText,
        closeButton,
        continueText,
    } = styles;

    return (
        <>
            <View style={globalStyles.paddingTop3}>
                <Text style={[globalStyles.paddingBottom1, styles.inputLabel]}>Closing Time</Text>
                <Pressable onPress={() => inputEditable && setClosingTimeModalVisible(!closingTimeModalVisible)}>
                    <TextInput
                        value={closingTime}
                        placeholder={"Closing Time"}
                        editable={false}
                        style={styles.inputField}
                        keyboardType={"default"}
                    />
                </Pressable>
            </View>
            <Modal
                isVisible={closingTimeModalVisible}
                style={modalView}>
                <View>
                    <View style={modalBody}>
                        <View style={monthArea}>
                            <Text style={monthAreaHeaderText}>Closing Time</Text>
                            <ScrollView style={monthAreaContent}>
                                {timings.map((item, index) => (
                                    <TouchableOpacity
                                        style={monthAreaContentBlock}
                                        key={index}
                                        onPress={() => setState({...state, closingTime: item})}
                                    >
                                        <Text
                                            style={[monthAreaContentText, closingTime === item && monthAreaContentTextActive]}>
                                            {item}
                                        </Text>
                                    </TouchableOpacity>
                                ))}
                            </ScrollView>
                        </View>
                    </View>
                    <View style={modalFooter}>
                        <TouchableOpacity
                            style={closeButton}
                            onPress={() => setClosingTimeModalVisible(!closingTimeModalVisible)}
                        >
                            <Text style={continueText}>Ok</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </>
    );
};

const styles = StyleSheet.create({
    modalView: {
        marginVertical: hp("0%"),
        marginHorizontal: wp("10%"),
        justifyContent: "center",
    },
    modalBody: {
        backgroundColor: "#fff",
        paddingVertical: hp("2%"),
        paddingHorizontal: wp("5%"),
        flexDirection: "row",
        justifyContent: "center",
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    modalFooter: {
        paddingVertical: hp("2%"),
        backgroundColor: "#fff",
        flexDirection: "row",
        justifyContent: "center",
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
    },
    closeButton: {
        backgroundColor: "#D2181B",
        paddingVertical: hp("1%"),
        paddingHorizontal: wp("5%"),
        borderRadius: 8,
    },
    monthArea: {
        width: wp("35%"),
    },
    monthAreaHeaderText: {
        fontSize: 18,
        paddingBottom: wp("2%"),
        paddingRight: wp("5%"),
    },
    monthAreaContent: {
        height: 100,
    },
    monthAreaContentBlock: {
        width: wp("30%"),
    },
    monthAreaContentText: {
        fontSize: 16,
        color: "#000",
        paddingVertical: hp("0.5%"),
        borderRadius: 8,
        paddingLeft: wp("2%"),
    },
    monthAreaContentTextActive: {
        backgroundColor: "#D2181B",
        color: "#fff",
        paddingLeft: wp("2%"),
    },
    continueText: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "700",
        textAlign: "center",
    },
    inputField: {
        paddingVertical: hp("1%"),
        paddingLeft: wp("4%"),
        borderColor: "#d9d3d3",
        borderWidth: 1,
        borderRadius: 8,
    },
    inputLabel: {
        color: "#555555",
    },
});

export default ClosingTimeModal;
