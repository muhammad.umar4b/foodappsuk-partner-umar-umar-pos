import React from "react";
import { ScrollView, Text, TouchableOpacity, View, TextInput, Pressable } from "react-native";

import Modal from "react-native-modal";

import styles from "../styles";
import globalStyles from "../../../assets/styles/globalStyles";

const timings = [
    "45",
    "60",
    "90",
];

const DeliveryTimeModal = (props) => {
    const {
        deliveryTimeModalVisible,
        setDeliveryTimeModalVisible,
        state,
        setState,
        inputEditable,
    } = props;

    const {
        restaurantDeliveryTime,
    } = state;

    const {
        modalView,
        modalBody,
        modalFooter,
        monthArea,
        monthAreaContent,
        monthAreaContentBlock,
        monthAreaContentText,
        monthAreaContentTextActive,
        monthAreaHeaderText,
        closeButton,
        continueText,
        inputLabel,
        inputField
    } = styles;

    const {
        paddingBottom1,
        paddingTop3
    } = globalStyles;

    return (
        <>
            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>Delivery Time</Text>
                <Pressable onPress={() => inputEditable && setDeliveryTimeModalVisible(!deliveryTimeModalVisible)}>
                    <TextInput
                        value={restaurantDeliveryTime}
                        placeholder={"Delivery Time"}
                        editable={false}
                        style={inputField}
                        keyboardType={"default"}
                    />
                </Pressable>
            </View>
            <Modal
                isVisible={deliveryTimeModalVisible}
                style={modalView}>
                <View>
                    <View style={modalBody}>
                        <View style={monthArea}>
                            <Text style={monthAreaHeaderText}>Delivery Time</Text>
                            <ScrollView style={monthAreaContent}>
                                {timings.map((item, index) => (
                                    <TouchableOpacity
                                        style={monthAreaContentBlock}
                                        key={index}
                                        onPress={() => setState({ ...state, restaurantDeliveryTime: item })}
                                    >
                                        <Text
                                            style={[monthAreaContentText, restaurantDeliveryTime === item && monthAreaContentTextActive]}>
                                            {item}
                                        </Text>
                                    </TouchableOpacity>
                                ))}
                            </ScrollView>
                        </View>
                    </View>
                    <View style={modalFooter}>
                        <TouchableOpacity
                            style={closeButton}
                            onPress={() => setDeliveryTimeModalVisible(!deliveryTimeModalVisible)}
                        >
                            <Text style={continueText}>Ok</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </>
    );
};

export default DeliveryTimeModal;
