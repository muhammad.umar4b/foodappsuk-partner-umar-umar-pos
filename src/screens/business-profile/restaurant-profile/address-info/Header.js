import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

import Feather from "react-native-vector-icons/Feather";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

import restaurantImage from "../../../../../assets/resturent.png";

const Header = (props) => {
    const {
        state,
        inputEditable,
        updateRestaurantAddress,
    } = props;

    const {
        restaurantLogo,
        circleIconArea,
        cardHeaderLabel,
        cardHeaderEditText,
    } = styles;

    const {
        boxShadow,
        cardHeader,
        elevation5,
        marginLeft06,
        marginRight1,
        paddingTop05,
        flexDirectionRow,
    } = globalStyles;

    const {
        name,
        logo,
    } = state;

    return (
        <View style={cardHeader}>
            <View style={flexDirectionRow}>
                <View style={[circleIconArea, boxShadow, elevation5, marginLeft06]}>
                    <Image source={logo ? { uri: logo } : restaurantImage} style={restaurantLogo} />
                </View>
                <Text style={cardHeaderLabel}>{name}</Text>
            </View>

            <TouchableOpacity style={[flexDirectionRow, paddingTop05]} onPress={() => updateRestaurantAddress()}>
                {!inputEditable &&
                    <MaterialIcons name="edit" size={20} color="#555555" />
                }

                {inputEditable &&
                    <Feather style={marginRight1} name="check-circle" size={20} color="#555555" />
                }

                <Text style={cardHeaderEditText}>
                    {inputEditable ? "Save" : "Edit"}
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default Header;
