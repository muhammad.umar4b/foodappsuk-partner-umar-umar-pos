import React from "react";
import { Text, TextInput, View } from "react-native";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

import SwitchButton from "../SwitchButton";

const Body = (props) => {
    const {
        state,
        setState,
        inputEditable,
    } = props;

    const {
        inputLabel,
        inputField,
    } = styles;

    const {
        paddingTop3,
        paddingBottom1,
    } = globalStyles;

    const {
        isSundayAvailable,
        isMondayAvailable,
        isTuesdayAvailable,
        isWednesdayAvailable,
        isThursdayAvailable,
        isFridayAvailable,
        isSaturdayAvailable,
        deliveryTimes,
        collectionTime,
        bookingStartTime,
        bookingEndTime,
    } = state;

    return (
        <>
            <SwitchButton
                state={state}
                label={"Monday"}
                setState={setState}
                disabled={!inputEditable}
                value={isSundayAvailable}
                name={"isSundayAvailable"}
            />
        </>
    );
};

export default Body;
