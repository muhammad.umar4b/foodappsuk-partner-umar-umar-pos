import React, {Component} from 'react';
import {
  FlatList,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import {Body, Card, CardItem} from 'native-base';
import {BLEPrinter} from 'react-native-thermal-receipt-printer';
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import axios from 'axios';
import moment from 'moment';
import {connect} from 'react-redux';
import {apiBaseUrl} from '../../config/index-example';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';

export class PendingOrders extends Component {
  state = {
    orders: [],
    resturentName: '',
    resturentLocation: '',
    orderNumber: '',
    name: '',
    userAddress: '',
    userPhone: '',
    orderDate: '',
    totalPrice: '',
    printing: true,
    printers: [],
    currentPrinter: '',
    foodItems: [],
    paymentMethod: '',
    deliveryCharge: '',
    discountCharge: '',
    discount: '',
  };

  getPrintContentGapText = size => {
    let i;
    let gap = '';
    for (i = 0; i < size; i++) {
      gap += ' ';
    }
    return gap;
  };

  getPrintContentGap = item => {
    const {title, quantity, price} = item;
    const leftContentLength = title.length + quantity.toString().length + 2; // Here 2 for parenthesis
    const rightContentLength =
      price.toString().length === 1
        ? price.toString().length + 3
        : parseFloat(price).toFixed(2).toString().length;
    return this.getPrintContentGapText(
      32 - (leftContentLength + rightContentLength),
    ); // Here 32 for page maximum character size
  };

  getSubtotalContentGap = (content, price) => {
    const leftContentLength = content.length;
    const rightContentLength =
      price.toString().length === 1
        ? price.toString().length + 3
        : parseFloat(price).toFixed(2).toString().length;
    return this.getPrintContentGapText(
      32 - (leftContentLength + rightContentLength),
    ); // Here 32 for page maximum character size
  };

  printBillTest = () => {
    /*var dates = new Date(this.state.orderDate);
         this.state.currentPrinter && BLEPrinter.printBill("<CM>" + this.state.resturentName + "</CM>\n<C>Address:" + this.state.resturentLocation + "</C>\n<C>Date:" + dates.getDate().toString() + "/" + dates.getMonth().toString() + "/" + dates.getFullYear().toString() + "</C>\n\n<C>Order no:" + this.state.orderNumber + "</C>\n<C>" + "QTY" + " " + " " + " " + " " + "Description" + " " + " " + " " + " " + "Amount</C>\n____________________________\n\n" + this.state.foodItems.map(arr => "<C>" + arr.quantity.toString() + " " + " " + " " + " " + arr.title.toString() + " " + " " + " " + " " + " " + arr.price.toString() + "</C>\n") + "\n\n____________________________\n\n<C>GROSS TOTAL:" + " " + " " + " " + " " + " " + " " + this.state.totalPrice + "</C>\n<C>DISCOUNT CHARGES:" + " " + " " + " " + "" / +" " + " " + this.state.deliveryCharge + "  </C>\n<C>DELIVERY FEES:" + " " + " " + " " + " " + " " + this.state.deliveryCharge + "  </C>\n<C>SAVINGS</C>\n________________________________\n<L>  TOTAL PAYMENT (CASH/CARD)  </L><R>  " + this.state.totalPrice.toString() + "  </R>\n\n\n\n\n\n<C>DELIVERY TO: " + this.state.userName + ", " + this.state.userAddress + "</C>\n\n<C>OR,</C>\n\n\n<C>COLLECTION FOR CUSTOMER NAME</C>");*/

    const {
      resturentName,
      resturentLocation,
      orderDate,
      orderNumber,
      foodItems,
      totalPrice,
      discount,
      deliveryCharge,
      orderType,
      name,
      userAddress,
    } = this.state;

    let subTotal = 0;
    foodItems.forEach(
      item => (subTotal += parseFloat(item.price) * parseInt(item.quantity)),
    );

    const discountCharges = 0.5;
    // const totalPaymentCash  = (subTotal - discount) + deliveryCharge + discountCharges;
    const seperator = '--------------------------------';

    /* Print Text PrinterHeader */
    const restaurantNameText = `<CM>${resturentName || 'N/A'}</CM>\n`;
    const restaurantLocationText = `<C>${resturentLocation || 'N/A'}</C>\n`;
    const orderDateAndOrderNoText = `<C>${
      orderDate ? moment(orderDate).format('DD/MM/YYYY') : 'N/A'
    }, Order No: ${orderNumber || 'N/A'}</C>\n\n`;

    /* Description Body */
    const descriptionHeader = `Description(QTY)     Amount(GBP)\n${seperator}\n`;

    let descriptionBody = '';
    foodItems.forEach(item => {
      descriptionBody += `${item.title}(${
        item.quantity
      })${this.getPrintContentGap(item)}${parseFloat(item.price).toFixed(2)}\n`;
    });

    /* Sub Total Body */
    const subtotalText = `Sub Total${this.getSubtotalContentGap(
      'Sub Total',
      subTotal,
    )}${parseFloat(subTotal).toFixed(2)}\n`;
    const discountText = `Discount${this.getSubtotalContentGap(
      'Discount',
      discount,
    )}${parseFloat(discount).toFixed(2)}\n`;
    const deliveryChargeText = `Delivery Charges${this.getSubtotalContentGap(
      'Delivery Charges',
      deliveryCharge,
    )}${parseFloat(deliveryCharge).toFixed(2)}\n`;
    const discountChargeText = `Discount Charges${this.getSubtotalContentGap(
      'Discount Charges',
      discountCharges,
    )}${parseFloat(discountCharges).toFixed(2)}\n`;
    const totalPaymentText = `Total Payment (CASH)${this.getSubtotalContentGap(
      'Total Payment (CASH)',
      totalPrice,
    )}${parseFloat(totalPrice).toFixed(2)}\n\n`;

    /* Address */
    const deliveryTo = `<D>Delivery: ${userAddress || 'N/A'}</D>`;
    const collectionFor = `<D>Collection: ${name || 'N/A'}</D>`;

    const printTextHeader = `${restaurantNameText}${restaurantLocationText}${orderDateAndOrderNoText}`;
    const printTextDescriptionBody = `${descriptionHeader}${descriptionBody}${seperator}\n`;
    const subtotalBody = `${subtotalText}${discountText}${deliveryChargeText}${discountChargeText}${seperator}\n${totalPaymentText}`;
    const printAddress = orderType === 'delivery' ? deliveryTo : collectionFor;
    const printText = `${printTextHeader}${printTextDescriptionBody}${subtotalBody}${printAddress}`;

    this.state.currentPrinter && BLEPrinter.printBill(printText);
  };

  _connectPrinter = printer => {
    BLEPrinter.connectPrinter(printer)
      .then(value => {
        this.setState({currentPrinter: value});
      })
      .then(() => {
        this.printBillTest();
      })
      .then(() => {
        this.setState({printing: false});
        ToastAndroid.show(
          'Printing completed',
          ToastAndroid.CENTER,
          ToastAndroid.SHORT,
        );
      });
  };

  printContent = () => {
    BluetoothStateManager.enable().then(result => {
      BLEPrinter.init().then(() => {
        BLEPrinter.getDeviceList().then(value => {
          if (this.props.activeBluetoothPrinter === '') {
            ToastAndroid.show(
              'Set your printer first',
              ToastAndroid.CENTER,
              ToastAndroid.SHORT,
            );
          } else {
            this._connectPrinter(this.props.activeBluetoothPrinter);
          }
        });
      });
    });
  };

  removeOrder = id => {
    // fetch(this.props.uri+'order/remove/'+id).then(()=>{
    //     fetch(this.props.uri+'order/fetch-by-owner/'+this.props.userId).then((response)=>response.json()).then((responseJson)=>{
    //         var newArray = responseJson.filter((item)=>{
    //             return item.orderStatus == "recieved";
    //         })
    //         this.setState({orders:newArray});
    //         })
    // })
  };

  getAllPendingOrders = async () => {
    try {
      const res = await axios.get(
        `${this.props.uri}order/fetch-by-owner/${this.props.userId}`,
      );
      const updateOrders = res.data.filter(
        item => item['orderStatus'] === 'pending',
      );
      this.setState({orders: updateOrders});
      return true;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  acceptOrder = async (id, values) => {
    var newArray = [];
    for (var i = 0; i < values.foodItems.length; i++) {
      var modifiedArray = [
        values.foodItems[i].quantity,
        values.foodItems[i].title,
        '£ ' +
          parseInt(values.foodItems[i].price) *
            parseInt(values.foodItems[i].quantity),
      ];
      newArray.push(modifiedArray);
    }
    this.setState({
      resturentName: values.restaurant.name,
      orderNumber: values.orderNumber,
      resturentLocation:
        values.restaurant.address + ', ' + values.restaurant.postCode,
      orderDate: values.createdAt,
      totalPrice: values.totalPrice,
      paymentMethod: values.paymentMethod,
      deliveryCharge: values.deliveryCharge,
      discount: values.discount,
      discountCharge: values.restaurant.deliveryCharges,
      name: values.user.name,
      userAddress: values.user.address + ', ' + values.user.postCode,
      userPhone: values.user.mobile,
      tableData: newArray,
      foodItems: values.foodItems,
      orderType: values.orderType,
    });

    try {
      const response = await axios.post(
        `${apiBaseUrl}order/accept-order-by-owner`,
        {orderId: id},
      );
      if (response.data) {
        showToastWithGravityAndOffset(response.data.data);
        this.printContent();
        await this.getAllPendingOrders();
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }

    /*fetch(this.props.uri + "order/accept-order-by-owner", {
            method : "POST",
            headers: {
                "Accept"      : "application/json",
                "Content-Type": "application/json",
            },
            body   : JSON.stringify({
                                        ,
                                    }),
        }).then((response) => response.json()).then(res => {

        }).catch((error) => {
            console.log(error);
        });*/
  };

  declineOrder = async id => {
    try {
      const response = await axios.delete(
        `${this.props.uri}order/remove/${id}`,
      );
      if (response.data) {
        await this.getAllPendingOrders();
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  fetchTimeDate = timestamp => {
    var iteim = new Date(timestamp).getDate();
    return iteim;
  };

  fetchTimeMonth = timestamp => {
    var iteim = new Date(timestamp).getMonth();
    return iteim;
  };

  fetchTimeYear = timestamp => {
    var iteim = new Date(timestamp).getYear();
    return iteim;
  };

  componentDidMount = async () => {
    await this.getAllPendingOrders();
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#ffffff'}}>
        {this.state.orders.length < 1 ? (
          <Card>
            <CardItem>
              <Body>
                <Text style={{color: '#646464', fontSize: 15}}>
                  There is no order request
                </Text>
              </Body>
            </CardItem>
          </Card>
        ) : (
          <FlatList
            data={this.state.orders}
            renderItem={({item}) => (
              <Card>
                <CardItem>
                  <Body>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: '100%',
                        paddingTop: 15,
                      }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        {item.orderNumber}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: '100%',
                        paddingTop: 15,
                      }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        Order Date
                      </Text>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        {moment(item.createdAt).format('DD/MM/YYYY')}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: '100%',
                        paddingTop: 15,
                      }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        Delivery Time
                      </Text>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        {item.deliveryTime}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 2,
                        backgroundColor: '#646464',
                        marginTop: 15,
                        marginBottom: 15,
                      }}></View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: '100%',
                        paddingTop: 15,
                      }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        Total Price
                      </Text>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'bold',
                          color: '#646464',
                        }}>
                        £{item.totalPrice}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        padding: 5,
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.declineOrder(item._id);
                        }}
                        style={{
                          padding: 10,
                          backgroundColor: 'black',
                          margin: 10,
                          elevation: 10,
                        }}>
                        <Text style={{color: 'white'}}>DECLINE</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          this.acceptOrder(item._id, item);
                        }}
                        style={{
                          padding: 10,
                          backgroundColor: '#d2181b',
                          margin: 10,
                          elevation: 10,
                        }}>
                        <Text style={{color: 'white'}}>ACCEPT</Text>
                      </TouchableOpacity>
                    </View>
                  </Body>
                </CardItem>
              </Card>
            )}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    userId: state.auth.userId,
    uri: state.auth.uri,
    activeBluetoothPrinter: state.auth.activeBluetoothPrinter,
    printerType: state.auth.printer,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(PendingOrders);
