import {View, Text} from 'react-native';
import {openApp} from 'rn-openapp';

import React from 'react';
import {StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import Clipboard from '@react-native-community/clipboard';
import AsyncStorage from '@react-native-async-storage/async-storage';
const InHouseSumupPayment = ({navigation, route}) => {
  const RandomNumber = Math.floor(Math.random() * 1000000);
  console.log(RandomNumber);
  const {orderPayload} = route.params;
  const totalPrice = orderPayload.totalPrice;
  const date = Date.now();
  var uCode = '';

  console.log(date);

  const copyToClipboard = async () => {
    await getUniqueCode();

    const data = `MJKUR_${uCode}_${date}`;
    console.log(data);
    Clipboard.setString(`${data}`);
    // ToastAndroid.showWithGravityAndOffset(
    //   `${RandomNumber} Copied To Clipboard`,
    //   ToastAndroid.LONG,
    //   ToastAndroid.BOTTOM,
    //   25,
    //   50,
    // );
  };

  const getUniqueCode = async () => {
    // var code = '';
    let code = await AsyncStorage.getItem('uCode');
    console.log('promise: ' + code);

    if (!code) {
      AsyncStorage.setItem('uCode', RandomNumber.toString());
      uCode = RandomNumber;
    } else {
      uCode = code;
    }

    // . then(cod => {
    //   if (!cod) {
    //     AsyncStorage.setItem('uCode', RandomNumber.toString());
    //     // code = RandomNumber;
    //     uCode = RandomNumber;
    //     console.log('tagCod: ' + cod);
    //   } else {
    //     uCode = cod;
    //     console.log('tagCod: ' + cod);
    //   }
    // });
    // console.log('tagCode: ' + uCode);
  };
  // getUniqueCode();

  copyToClipboard();
  const onPressSumupHandler = async amount => {
    console.log(`My Price ${amount}`);
    const res = await fetch(
      `https://food-apps-uk-default-rtdb.firebaseio.com/sumup/${uCode}/${date.toString()}.json`,
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          amount,

          isCharged: false,
        }),
      },
    );
    console.log(res);
    const examplePackageId = 'com.sumup.sdksampleapp';
    openApp(examplePackageId)
      .then(result => console.log(result))
      .catch(e => console.warn(e));
    if (orderPayload.screenType === 'paymentScreen')
      navigation.navigate('RestaurantDetails', {uCode, date});
    if (orderPayload.screenType === 'inHouseDineInPayment')
      navigation.navigate('InHouseDineInPayment', {uCode, date});
  };
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Total Amount</Text>
      <Text style={styles.text}>£ {totalPrice}</Text>
      <Button
        buttonStyle={styles.button}
        onPress={() => onPressSumupHandler(totalPrice)}
        title="Sumup Payment"
        accessibilityLabel="Sumup Payment"
      />
      {/* <Text style={styles.text1}>
        Click On The Below Code To Copy On Clipboard
      </Text>
      <TouchableOpacity
        style={{marginTop: 5}}
        onPress={() => copyToClipboard()}>
        <Text style={styles.text2}> {RandomNumber}</Text>
      </TouchableOpacity> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',

    alignItems: 'center',
  },
  text: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
  },
  text2: {
    padding: 5,
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
    backgroundColor: '#d2181b',
    color: '#fff',
    paddingRight: 10,
    borderRadius: 5,
  },

  text1: {
    marginTop: 30,
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
    color: 'red',
  },
  button: {
    marginTop: 20,
    borderRadius: 10,
    fontSize: 32,
    fontWeight: 'bold',
    alignItems: 'center',
    backgroundColor: '#d2181b',
    padding: 10,
  },
});
export default InHouseSumupPayment;
