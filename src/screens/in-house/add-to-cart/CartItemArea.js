import React from 'react';
import {ScrollView, Text, View} from 'react-native';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

import CartItem from './CartItem';

export default function CartItemArea(props) {
  const {cartList, addToCart, removeFromCart} = props;

  const {addedCartAreaHeader, addedCartAreaHeaderText} = styles;

  const {marginTop2, paddingLeft5} = globalStyles;

  return (
    <View>
      <View style={addedCartAreaHeader}>
        <Text style={addedCartAreaHeaderText}>
          {cartList ? cartList.foodItems.length : 0} items
        </Text>
      </View>
      <ScrollView>
        {cartList ? (
          cartList.foodItems.map((item, index) => (
            <CartItem
              key={index}
              index={index}
              item={item}
              addToCart={addToCart}
              removeFromCart={removeFromCart}
            />
          ))
        ) : (
          <View style={[marginTop2, paddingLeft5]}>
            <Text>No item found!</Text>
          </View>
        )}
      </ScrollView>
    </View>
  );
}
