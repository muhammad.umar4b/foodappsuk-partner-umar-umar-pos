import React, {useState} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';
import {SET_CART_LIST} from '../../../store/types';
import {Colors} from 'react-native/Libraries/NewAppScreen';
// import OrderSummary from '../../../utilities/order-summary/OrderSummary';

// //////////////////////////////////////////////////////////////////////////

export default function FormArea(props) {
  const {state, cartList, dispatch} = props;

  const {
    kitchenNoteText,
    kitchenNoteInput,
    extraKitchenNote,
    kitchenNoteButton,
    kitchenNoteButtonArea,
  } = styles;

  const {paddingHorizontal5, bgDisabledGrey} = globalStyles;

  const {subTotal} = state;

  return (
    <View style={paddingHorizontal5}>
      <View>
        <Text style={extraKitchenNote}>Extra Allergy Note</Text>
        <TextInput
          value={cartList ? cartList.note : ''}
          onChangeText={value =>
            dispatch({type: SET_CART_LIST, payload: {...cartList, note: value}})
          }
          style={kitchenNoteInput}
          keyboardType={'default'}
          editable={!!cartList}
        />
      </View>
    </View>
  );
}
