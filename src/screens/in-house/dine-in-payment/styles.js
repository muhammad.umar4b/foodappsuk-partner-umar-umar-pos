import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    subContainer: {
        flex: 1,
        justifyContent: "space-between",
    },
    orderSummaryArea: {
        borderWidth: 1,
        borderColor: "#b4b4b4",
        paddingHorizontal: wp("3%"),
        paddingVertical: hp("1%"),
        borderRadius: 8,
        marginTop: hp("1%"),
    },
    cartListArea: {
        borderBottomColor: "#b4b4b4",
        borderBottomWidth: 1,
        paddingVertical: hp("0.5%"),
    },
    totalPayment: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderTopWidth: 1,
        borderColor: "#b4b4b4",
        paddingTop: hp("0.5%"),
    }
});

export default styles;
