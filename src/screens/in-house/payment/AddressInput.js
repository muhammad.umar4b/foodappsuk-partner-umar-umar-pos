import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

import {calculateDeliveryCharge} from '../../../store/actions/order';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

const AddressInput = props => {
  const {
    state,
    setState,
    address,
    postCode,
    restaurantData,
    deliveryAddressInputEditable,
    setDeliveryAddressInputEditable,
  } = props;

  const addDeliveryAddress = () => {
    if (deliveryAddressInputEditable) {
      if (!address) {
        showToastWithGravityAndOffset('Address field is required!');
        return false;
      }

      if (!postCode) {
        showToastWithGravityAndOffset('Post Code field is required!');
        return false;
      }

      const payload = {
        postCode,
        restaurantPostCode: restaurantData.postCode,
        restaurant: restaurantData._id,
      };

      calculateDeliveryCharge(payload).then(res => {
        console.log('DELIVERY CHARGE: ', !!res);
        if (res) setState({...state, deliveryCharge: res});
      });
    }

    setDeliveryAddressInputEditable(!deliveryAddressInputEditable);
  };

  const {
    cardAddress,
    boxShadow,
    marginTop3,
    marginRight1,
    cardHeader,
    paddingTop05,
    paddingTop3,
    flexDirectionRow,
    paddingHorizontal5,
    paddingTop02,
    paddingLeft05,
    paddingTop2,
  } = globalStyles;

  const {inputField, cardHeaderLabel, cardHeaderEditText} = styles;

  return (
    <View>
      <View style={[cardAddress, boxShadow, marginTop3]}>
        <View style={cardHeader}>
          <View style={flexDirectionRow}>
            <Ionicons
              style={paddingTop02}
              name="md-location-outline"
              size={26}
              color="#D2181B"
            />
            <Text style={[cardHeaderLabel, paddingLeft05]}>Address</Text>
          </View>
          <TouchableOpacity
            style={[flexDirectionRow, {paddingTop: 8}, {marginRight: 7}]}
            onPress={() => addDeliveryAddress()}>
            {!deliveryAddressInputEditable && (
              <MaterialIcons name="edit" size={20} color="#555555" />
            )}

            {deliveryAddressInputEditable && (
              <Feather
                style={marginRight1}
                name="check-circle"
                size={20}
                color="#555555"
              />
            )}
            <Text style={cardHeaderEditText}>
              {deliveryAddressInputEditable ? 'Save' : 'Edit'}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={paddingTop3}>
          <TextInput
            value={address}
            onChangeText={value => setState({...state, address: value})}
            editable={deliveryAddressInputEditable}
            style={inputField}
            keyboardType={'default'}
            placeholder={'Address'}
            multiline={true}
            numberOfLines={4}
          />
        </View>
        <View style={paddingTop2}>
          <TextInput
            value={postCode}
            onChangeText={value => setState({...state, postCode: value})}
            editable={deliveryAddressInputEditable}
            style={inputField}
            keyboardType={'default'}
            placeholder={'Post Code'}
          />
        </View>
      </View>
    </View>
  );
};

export default AddressInput;
