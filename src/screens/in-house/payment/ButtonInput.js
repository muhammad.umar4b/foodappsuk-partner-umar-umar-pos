import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import {useState} from 'react';

// import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

// import ButtonLoader from '../../../utilities/ButtonLoader';

const ButtonInput = props => {
  const {placeOrder, isLoading, service_type, savedDineIn} = props;

  const {flexDirectionRow, paddingHorizontal5, marginLeft2} = globalStyles;
  let [buttonState, setButtonState] = useState('false');

  // const {continueButtonArea, continueButton, continueText} = styles;

  return (
    <View>
      <View>
        <TouchableOpacity
          style={buttonState === 'false' ? styles1.button : styles1.button2}
          onPress={() => {
            placeOrder('save').then(
              res => console.log('ORDER RESPONSE: ', res),
              setButtonState('true'),
            );
          }}>
          <Text style={{color: '#fff'}}>
            {service_type === 'dine_in' ? 'Save' : 'Checkout'}
          </Text>
        </TouchableOpacity>

        {service_type === 'dine_in' && savedDineIn && (
          <TouchableOpacity
            style={buttonState === 'false' ? styles1.button : styles1.button2}
            onPress={() => {
              placeOrder('serve').then(
                res => console.log('ORDER RESPONSE: ', res),
                setButtonState('true'),
              );
            }}>
            <Text style={{color: '#fff'}}>Serve</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default ButtonInput;

const styles1 = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   paddingHorizontal: 10,
  // },

  button: {
    // flex: 1,
    width: 150,
    // alignItems: 'center',
    backgroundColor: '#D2181B',
    justifyContent: 'center',
    alignItems: 'center',

    padding: 10,
    margin: 3,
    borderRadius: 5,
    maxHeight: 48,
    minHeight: 48,
  },
  button2: {
    // flex: 1,
    width: 150,
    // alignItems: 'center',
    backgroundColor: '#00be51',
    justifyContent: 'center',
    alignItems: 'center',

    padding: 10,
    margin: 3,
    borderRadius: 5,
    maxHeight: 48,
    minHeight: 48,
  },
});
