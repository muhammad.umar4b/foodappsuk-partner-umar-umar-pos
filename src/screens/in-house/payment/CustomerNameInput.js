import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

const CustomerNameInput = props => {
  const {
    state,
    setState,
    isDineIn,
    customerName,
    customerInputEditable,
    setCustomerInputEditable,
  } = props;

  const {
    card2,
    boxShadow,
    marginTop3,
    marginRight1,
    cardHeader,
    elevation5,
    marginLeft06,
    paddingTop05,
    paddingTop3,
    flexDirectionRow,
    paddingHorizontal5,
  } = globalStyles;

  const {inputField, circleIconArea, cardHeaderLabel, cardHeaderEditText} =
    styles;

  return (
    <View>
      {/* <View style={[card2]}>
        <View style={cardHeader}> */}
      {/* <View style={flexDirectionRow}>
            <View
              style={[circleIconArea, boxShadow, elevation5, {marginTop: 5}]}>
              <Feather name="user" size={20} color="#D2181B" />
            </View>
            <Text style={cardHeaderLabel}>Name</Text>
          </View> */}
      {/*{!isDineIn &&
                        <TouchableOpacity style={[flexDirectionRow, paddingTop05]}
                                          onPress={() => setCustomerInputEditable(!customerInputEditable)}>
                            {!customerInputEditable && <MaterialIcons name="edit" size={20} color="#555555" />}
                            {customerInputEditable &&
                                <Feather style={marginRight1} name="check-circle" size={20} color="#555555" />
                            }
                            <Text style={cardHeaderEditText}>
                                {customerInputEditable ? "Save" : "Edit"}
                            </Text>
                        </TouchableOpacity>
                    }*/}
      {/* </View> */}
      <View>
        <TextInput
          value={customerName}
          onChangeText={value => setState({...state, customerName: value})}
          editable={true}
          style={inputField}
          keyboardType={'default'}
          placeholder={
            isDineIn ? 'Enter Customer Name ' : 'Enter Customer Name'
          }
        />
      </View>
    </View>
    // </View>
  );
};

export default CustomerNameInput;
