import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

const PhoneNumberInput = props => {
  const {
    state,
    setState,
    phoneNumber,
    isDineIn,
    contactInputEditable,
    setContactInputEditable,
  } = props;

  const {
    card2,
    boxShadow,
    marginTop3,
    marginRight1,
    cardHeader,
    elevation5,
    marginLeft06,
    paddingTop05,
    paddingTop3,
    flexDirectionRow,
    paddingHorizontal5,
  } = globalStyles;

  const {inputField, circleIconArea, cardHeaderLabel, cardHeaderEditText} =
    styles;

  return (
    <View>
      <TextInput
        value={phoneNumber}
        onChangeText={value => setState({...state, phoneNumber: value})}
        editable={true}
        style={inputField}
        keyboardType={'default'}
        placeholder={isDineIn ? 'Enter Guest Number ' : 'Enter Guest Number'}
      />
    </View>
    // <View style={[card2]}>
    // {/* <View style={cardHeader}> */}
    // {/* <View style={flexDirectionRow}>
    //   <View style={[circleIconArea, boxShadow, elevation5, {marginTop: 5}]}>
    //     <Feather name="smartphone" size={20} color="#D2181B" />
    //   </View>
    //   <Text style={cardHeaderLabel}>Number</Text>
    // </View> */}
    //   {/*{!isDineIn &&
    //                   <TouchableOpacity style={[flexDirectionRow, paddingTop05]}
    //                                     onPress={() => setContactInputEditable(!contactInputEditable)}>
    //                       {!contactInputEditable && <MaterialIcons name="edit" size={20} color="#555555" />}
    //                       {contactInputEditable &&
    //                           <Feather style={marginRight1} name="check-circle" size={20} color="#555555" />
    //                       }
    //                       <Text style={cardHeaderEditText}>
    //                           {contactInputEditable ? "Save" : "Edit"}
    //                       </Text>
    //                   </TouchableOpacity>
    //               }*/}
    // {/* </View> */}

    // </View>
  );
};

export default PhoneNumberInput;
