import React from 'react';
import {Image, Pressable, Text, TextInput, View} from 'react-native';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

// Image
import tableBookingImage from '../../../../assets/table-booking.png';

const TableNumberInput = props => {
  const {
    tableNo,
    savedDineIn,
    isModalVisible,
    setModalVisible,
    state,
    setState,
  } = props;

  const {
    card2,
    boxShadow,
    marginTop3,
    cardHeader,
    elevation5,
    marginLeft06,
    paddingTop3,
    flexDirectionRow,
    paddingHorizontal5,
  } = globalStyles;

  const {inputField, circleIconArea, cardHeaderLabel, bookingIconImage} =
    styles;

  return (
    <View>
      {/* <View>
        <View style={flexDirectionRow}>
          <View style={[circleIconArea, boxShadow, elevation5, {marginTop: 5}]}>
            <Image source={tableBookingImage} style={bookingIconImage} />
          </View>
          <Text style={cardHeaderLabel}>Table No</Text>
        </View>
      </View> */}

      {/*<Pressable onPress={() => {
                        if (savedDineIn) {
                            alert("Table Number is already assigned!");
                            return;
                        }
                        setModalVisible(!isModalVisible);
                    }}>*/}
      <TextInput
        value={tableNo.toString()}
        editable={!savedDineIn}
        style={inputField}
        keyboardType={'default'}
        placeholder={'Enter Table Number (Required)'}
        onChangeText={value => setState({...state, tableNo: value})}
      />
      {/*</Pressable>*/}
    </View>
  );
};

export default TableNumberInput;

// import React from 'react';
// import {Image, Pressable, Text, TextInput, View} from 'react-native';

// import styles from './styles';
// import globalStyles from '../../../assets/styles/globalStyles';

// // Image
// import tableBookingImage from '../../../../assets/table-booking.png';

// const TableNumberInput = props => {
//   const {tableNo, savedDineIn, isModalVisible, setModalVisible} = props;

//   const {
//     card2,
//     boxShadow,
//     cardHeader,
//     elevation5,
//     marginLeft06,
//     paddingTop3,
//     flexDirectionRow,
//   } = globalStyles;

//   const {inputField, circleIconArea, cardHeaderLabel, bookingIconImage} =
//     styles;

//   return (
//     <View>
//       <View style={[card2]}>
//         <View style={cardHeader}>
//           <View style={flexDirectionRow}>
//             <View style={[circleIconArea, boxShadow, elevation5, marginLeft06]}>
//               <Image source={tableBookingImage} style={bookingIconImage} />
//             </View>
//             <Text style={cardHeaderLabel}>Table Numbers</Text>
//           </View>
//         </View>
//         <View style={paddingTop3}>
//           <Pressable
//             onPress={() => {
//               if (savedDineIn) {
//                 alert('Table Number is already assigned!');
//                 return;
//               }
//               setModalVisible(!isModalVisible);
//             }}>
//             <TextInput
//               value={tableNo.toString()}
//               editable={false}
//               style={inputField}
//               keyboardType={'default'}
//               placeholder={'Enter Table No'}
//             />
//           </Pressable>
//         </View>
//       </View>
//     </View>
//   );
// };

// export default TableNumberInput;
