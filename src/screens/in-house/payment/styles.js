import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 100,
    backgroundColor: '#fff',
    marginLeft: 1,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  rightView: {
    // display: 'flex',
    width: '49%',
  },
  LeftView: {
    width: '49%',
  },
  //   leftView: {
  //     // width: '40%',
  //     // display: 'flex',
  //     // flex: 2,
  //     margin: 0,
  //     padding: 0,
  //     borderColor: 'green',
  //     borderWidth: 2,
  //   },
  circleIconArea: {
    backgroundColor: '#fff',
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardHeaderLabel: {
    color: '#555555',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5,
    marginRight: 5,
    paddingLeft: wp('2%'),
    paddingTop: hp('0.5%'),
  },
  cardHeaderEditText: {
    color: '#555555',
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: wp('0.3%'),
  },
  inputField: {
    width: '100%',
    paddingVertical: hp('3%'),
    textAlign: 'center',
    // paddingLeft: wp('4%'),
    borderColor: '#d9d3d3',
    borderWidth: 1,
    borderRadius: 8,
  },
  contactText: {
    fontSize: 16,
    fontWeight: '600',
    paddingBottom: hp('1%'),
  },
  continueButtonArea: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: hp('3%'),
    marginBottom: hp('8%'),
  },
  continueButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1.4%'),
    marginLeft: 2,
    marginTop: 4,
    textAlign: 'center',
    marginBottom: 0.5,
    // paddingHorizontal: wp('10%'),
    // width: '20%',
    // padding: 10,
    borderRadius: 5,
    width: 150,
  },
  continueText: {
    width: 150,
    color: '#fff',
    fontSize: 14,
    fontWeight: '700',
    textAlign: 'center',
    padding: 3,
  },
  bookingIconImage: {
    width: 20,
    height: 20,
  },
  modalView: {
    marginVertical: hp('0%'),
    marginHorizontal: wp('10%'),
    justifyContent: 'center',
  },
  modalBody: {
    backgroundColor: '#fff',
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    flexDirection: 'row',
    justifyContent: 'center',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  modalFooter: {
    paddingVertical: hp('2%'),
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  closeButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1%'),
    paddingHorizontal: wp('5%'),
    borderRadius: 8,
  },
  monthArea: {
    width: wp('35%'),
  },
  monthAreaHeaderText: {
    fontSize: 18,
    paddingBottom: wp('2%'),
    paddingRight: wp('5%'),
  },
  monthAreaContent: {
    height: 100,
  },
  monthAreaContentBlock: {
    width: wp('30%'),
  },
  monthAreaContentText: {
    fontSize: 16,
    color: '#000',
    paddingVertical: hp('0.5%'),
    borderRadius: 8,
    paddingLeft: wp('2%'),
    textAlign: 'center',
  },
  monthAreaContentTextActive: {
    backgroundColor: '#D2181B',
    color: '#fff',
    paddingLeft: wp('2%'),
  },
});

export default styles;
