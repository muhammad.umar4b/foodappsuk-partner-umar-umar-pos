import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import moment from "moment";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

const SingleItem = (props) => {
    const {
        navigation,
        item,
        index,
        setStatusBgColor,
    } = props;

    const {
        _id,
        orderNumber,
        createdAt,
        orderStatus,
        paymentMethod,
        totalPrice,
        orderType,
        tableNo,
    } = item;

    const {
        statusButtonText,
        totalAmountArea,
        editButton,
        editButtonText,
    } = styles;

    const {
        card,
        boxShadow,
        marginTop2,
        flexDirectionRow,
        justifyBetween,
        textGrey,
        marginLeft2,
        marginBottom1,
        paddingTop1,
        paddingBottom2,
        textCapitalize,
    } = globalStyles;

    return (
        <View style={[card, boxShadow, marginTop2]}>
            <View style={[flexDirectionRow, justifyBetween, marginBottom1]}>
                <View style={[flexDirectionRow]}>
                    <Text style={textGrey}>{orderNumber}</Text>
                    <View style={[marginLeft2, setStatusBgColor(orderStatus), { height: 22, borderRadius: 5 }]}>
                        <Text style={statusButtonText}>{orderStatus}</Text>
                    </View>
                </View>

                {orderType === "Dine In" &&
                    <TouchableOpacity
                        onPress={() => navigation.navigate("RestaurantDetails", { orderDetailsId: _id })}
                        style={editButton}
                    >
                        <Text style={editButtonText}>
                            EDIT
                        </Text>
                    </TouchableOpacity>
                }
            </View>

            <TouchableOpacity
                onPress={() => navigation.navigate("InHouseOrderDetails", { id: _id })}
                key={index}
            >
                <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                    <Text>Order Date</Text>
                    <Text>{moment(createdAt).format("DD MMM YYYY, hh:mm A") || "N/A"}</Text>
                </View>

                <View
                    style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                    <Text>Payment Method</Text>
                    <Text style={textCapitalize}>{paymentMethod || "N/A"}</Text>
                </View>

                <View
                    style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                    <Text>Order Type</Text>
                    <Text style={textCapitalize}>{orderType || "N/A"}</Text>
                </View>

                {orderType === "Dine In" &&
                    <View
                        style={[flexDirectionRow, justifyBetween, paddingTop1, paddingBottom2]}>
                        <Text>Table No</Text>
                        <Text style={textCapitalize}>{tableNo || "N/A"}</Text>
                    </View>
                }

                <View style={totalAmountArea}>
                    <Text>Total Price</Text>
                    <Text>£{parseFloat(totalPrice).toFixed(2)}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default SingleItem;
