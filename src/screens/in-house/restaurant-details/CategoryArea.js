import {
  ScrollView,
  Text,
  View,
  Pressable,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';

import FoodItemArea from './FoodItemArea';
import Loader from '../../../utilities/Loader';

import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

// /////////////////////////////////////////////////////////////////////////////////////////////////////////

import React, {useEffect, useState} from 'react';
// import {SafeAreaView, ScrollView} from 'react-native';

import axios from 'axios';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {useDispatch, useSelector} from 'react-redux';
import styles1 from '../payment/styles';
// const {container, leftView, rightView} = styles;

import {apiBaseUrl} from '../../../config/index-example';
import {removeFromCart} from '../../../store/actions/cart';
import {validateInputField, getTotalPrice} from '../payment/utility';
import {getOrderDetailsAndPrint} from '../../../utilities/getOrderDetailsAndPrint';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

// Component
import ButtonInput from '../payment/ButtonInput';
import AddressInput from '../payment/AddressInput';
import PhoneNumberInput from '../payment/PhoneNumberInput';
import TableNumberInput from '../payment/TableNumberInput';
import PaymentTypeInput from '../payment/PaymentTypeInput';
import TableNumberModal from '../payment/TableNumberModal';
import CustomerNameInput from '../payment/CustomerNameInput';
import OrderSummary from '../../../utilities/order-summary/OrderSummary';
import {Image} from 'react-native-elements';

// Images
import cardImage from '../../../../assets/img/card.png';
// import {View} from 'native-base';

export default function CategoryArea(props) {
  // let [number, setNumber] = useState('false');
  // let [tablePos, setTablePos] = useState('false');
  // let [addressPos, setAddressPos] = useState('false');
  // let [modalVisible2, setModalVisible2] = useState(false);
  let [choice, setChoice] = useState([
    'Name',
    'Number',
    'TablePos',
    'AddressPos',
    'PaymentPos',
    '',
  ]);
  const {
    categoryList,
    toggleCategory,
    toggleAddToCart,
    isViewBasket,
    activeCategory,
    foodItemList,
    foodItemLoading,
    categoryListLoading,
    paymentState,
    setPaymentState,
    navigation,
    route,
  } = props;
  let isCharged = 'false';
  //Getting Values from InHouseSumupPayment
  let interval;
  React.useEffect(async () => {
    let {uCode, date} = route?.params || {};
    if (uCode) {
      try {
        // let charge = true;
        let i = 0;
        interval = setInterval(async () => {
          const response = await fetch(
            `https://food-apps-uk-default-rtdb.firebaseio.com/sumup/${uCode}/${date}.json`,
          );
          let data = await response.json();
          let charge = data.isCharged.toString();
          console.log(`This will run every second! ok ${charge}`);
          // charge=!charge
          if (charge === 'true') {
            isCharged = 'true';
            placeOrder('save').then(res =>
              console.log('ORDER RESPONSE: ', res),
            );

            clearInterval(interval);
          }
          if (i > 80) {
            clearInterval(interval);
            console.log('interval cleared!');
          }
          i++;
        }, 7000);
        return () => clearInterval(interval);
      } catch (e) {
        console.log(`Error for Fetching Data From Firebase `);
      }
    }
  }, [route.params]);

  const [isLoading, setIsLoading] = useState(false);
  const [paymentType, setPaymentType] = useState([
    {
      id: 1,
      value: 'Cash',
      name: 'Cash',
      img: null,
      icon: <FontAwesome5 name="cash-register" size={24} color="#524F4FFF" />,
      selected: false,
    },
    {
      id: 2,
      value: 'Card',
      name: 'Card',
      img: cardImage,
      selected: false,
    },
    {
      id: 3,
      value: 'Sumup',
      name: 'Sumup',
      img: null,
      icon: (
        <Image
          source={require('../../../../assets/img/sumup.png')}
          style={{height: 23, width: 40}}
        />
      ),
      selected: false,
    },
  ]);
  const [contactInputEditable, setContactInputEditable] = useState(false);
  const [customerInputEditable, setCustomerInputEditable] = useState(false);
  const [deliveryAddressInputEditable, setDeliveryAddressInputEditable] =
    useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [state, setState] = useState({
    customerName: '',
    address: '',
    postCode: '',
    phoneNumber: '',
    tableNo: '',
    deliveryCharge: 0,
  });

  const {restaurant} = useSelector(state => state);
  const dispatch = useDispatch();

  const {service_type, cartList, restaurantData, savedDineIn, orderDetails} =
    restaurant;

  const {
    customerName,
    address,
    postCode,
    phoneNumber,
    tableNo,
    deliveryCharge,
  } = state;

  useEffect(() => {
    if (savedDineIn && orderDetails) {
      const {customerName, mobileNo, tableNo} = orderDetails;

      setState({
        customerName,
        address: '',
        postCode: '',
        phoneNumber: mobileNo,
        tableNo,
      });
    }
  }, [savedDineIn, orderDetails]);

  const placeOrder = async (buttonType = '') => {
    const paymentOptions = paymentType.find(item => item.selected === true);

    const {_id, coupon, uniId, isRiderService, discount, serviceCharge} =
      restaurantData;

    const validationObj = {
      customerName,
      phoneNumber,
      service_type,
      tableNo,
      address,
      postCode,
      paymentOptions,
    };

    if (!validateInputField(validationObj)) return false;

    let payload = {
      restaurant: _id,
      restaurantUniId: uniId,
      coupon,
      customerName,
      mobileNo: phoneNumber,
      subTotal: cartList.subTotal,
      shippingFee: 0,
      screenType: 'paymentScreen',
      totalPrice: getTotalPrice(
        service_type,
        cartList.subTotal,
        serviceCharge,
        deliveryCharge,
      ),
      paymentMethod: paymentOptions ? paymentOptions.value : '',
      kitchenNotes: cartList.note,
      riderService: isRiderService,
      cartItems: JSON.stringify(cartList),
      postCode,
      fromRestaurant: true,
    };

    switch (service_type) {
      case 'dine_in':
        payload.orderType = 'Dine In';
        payload.tableNo = parseInt(tableNo);
        payload.serviceCharge = serviceCharge;

        delete payload.postCode;
        delete payload.shippingFee;
        delete payload.riderService;

        if (buttonType === 'serve') {
          navigation.navigate('InHouseDineInPayment');
          return false;
        }

        payload.paymentMethod = 'Counter';
        payload.paymentStatus = 'Pending';

        try {
          setIsLoading(true);

          if (savedDineIn && orderDetails) {
            const apiEndPoint = `order/update-inhouse-pendingOrder/${orderDetails._id}`;
            const dineInResponse = await axios.put(
              `${apiBaseUrl}${apiEndPoint}`,
              cartList,
            );

            if (dineInResponse.data) {
              removeFromCart(dispatch);
              setIsLoading(false);
              showToastWithGravityAndOffset('Order updated successfully!');
              navigation.navigate('InHouseOrder', {pending: true});
            }
          } else {
            const dineInResponse = await axios.post(
              `${apiBaseUrl}order/create-inhouse-dine-in`,
              payload,
            );

            if (dineInResponse.data) {
              removeFromCart(dispatch);
              setIsLoading(false);
              showToastWithGravityAndOffset('Order placed successfully!');

              navigation.navigate('RestaurantDetails', {pending: true});
              setPaymentState('false');
            }
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          setIsLoading(false);
        }
        break;
      case 'collection':
        payload.orderType = 'Collection';

        if (paymentOptions.value === 'Card') {
          navigation.navigate('InHouseCardPayment', {orderPayload: payload});
          return false;
        }
        if (paymentOptions.value === 'Sumup' && isCharged === 'false') {
          navigation.navigate('InHouseSumupPayment', {orderPayload: payload});
          return false;
        }

        try {
          setIsLoading(true);
          const collectionResponse = await axios.post(
            `${apiBaseUrl}order/create-inhouse-order`,
            payload,
          );

          if (collectionResponse.data) {
            removeFromCart(dispatch);
            setIsLoading(false);
            showToastWithGravityAndOffset('Order placed successfully!');

            getOrderDetailsAndPrint(
              collectionResponse.data.data._id,
              restaurantData,
              true,
            ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));
            navigation.navigate('RestaurantDetails', {pending: false});
            setPaymentState('false');
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          setIsLoading(false);
        }
        break;
      case 'delivery':
        payload.deliveredAt = address;
        payload.orderType = 'Home Delivery';
        payload.deliveryCharge = deliveryCharge;
        payload.totalPrice = getTotalPrice(
          service_type,
          cartList.subTotal,
          serviceCharge,
          deliveryCharge,
        );

        if (paymentOptions.value === 'Card') {
          navigation.navigate('InHouseCardPayment', {orderPayload: payload});
          return false;
        }
        if (paymentOptions.value === 'Sumup' && isCharged === 'false') {
          navigation.navigate('InHouseSumupPayment', {orderPayload: payload});
          return false;
        }

        try {
          setIsLoading(true);
          const deliveryResponse = await axios.post(
            `${apiBaseUrl}order/create-inhouse-order`,
            payload,
          );

          if (deliveryResponse.data) {
            removeFromCart(dispatch);
            setIsLoading(false);
            showToastWithGravityAndOffset('Order placed successfully!');
            getOrderDetailsAndPrint(
              deliveryResponse.data.data._id,
              restaurantData,
              true,
            ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));
            // navigation.navigate('InHouseOrder', {pending: false});
            navigation.navigate('RestaurantDetails', {pending: false});
            setPaymentState('false');
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          setIsLoading(false);
        }
        break;
      default:
        return true;
    }

    return true;
  };

  const onRadioBtnClick = item => {
    const updateState = paymentType.map(value =>
      item.id === value.id
        ? {...value, selected: true}
        : {...value, selected: false},
    );
    setPaymentType(updateState);
  };

  const isDineIn = service_type === 'dine_in';

  // ///////////////////////////////////////////////////////////////////////////////////////////////////////////

  const {
    categoryArea1,
    textCategory,
    container,
    text,
    text2,
    categoryView,
    categoryViewActive,
    categoryHeaderText,
  } = styles;

  const {background, boxShadow, textWhite, textGreen, f20, fw700, paddingTop5} =
    globalStyles;

  const customHeight = isViewBasket
    ? {height: hp('300%')}
    : {height: hp('300%')};

  return (
    <View>
      <View style={[styles.container]}>
        <View style={categoryArea1}>
          {categoryList.map((item, index) => (
            <View key={index}>
              <TouchableOpacity
                style={[
                  categoryView,
                  background,
                  item.isActive ? categoryViewActive : {},
                ]}
                onPress={() => toggleCategory(item._id)}>
                <Text style={[{textAlign: 'center'}, {color: '#fff'}]}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
        {categoryList.map((item, index) => (
          <View key={index}>
            {item.isActive && (
              <FoodItemArea
                foodItemLoading={foodItemLoading}
                activeCategory={activeCategory}
                toggleAddToCart={toggleAddToCart}
                foodItemList={foodItemList}
                // itemIsActive={item.isActive}
              />
            )}
            {/* {console.log('hello' + item.isActive)} */}
          </View>
        ))}
      </View>
      {categoryListLoading && (
        <View style={paddingTop5}>
          <Loader />
        </View>
      )}
      <View style={{marginTop: 100}}>
        <View
          style={{
            // flex: 1,
            display: 'flex',
            flexDirection: 'row',
            // flexWrap: 'wrap',
            justifyContent: 'flex-start',
            flexWrap: 'wrap',
            // alignItems: ,
            // width: '100%',
            // backgroundColor: '#212121',
            // marginBottom: 50,
          }}>
          {/* <View>
            <TouchableOpacity
              style={stylespayment.button}
              value={choice}
              onPress={() => setChoice('Number')}>
              <Text style={{color: '#fff'}}>Number</Text>
            </TouchableOpacity>
            {choice === 'Number' && (
              <PhoneNumberInput
                state={state}
                setState={setState}
                phoneNumber={phoneNumber}
                isDineIn={isDineIn}
                contactInputEditable={contactInputEditable}
                setContactInputEditable={setContactInputEditable}
              />
            )}
          </View> */}
          <View>
            <Modal
              animationType="slide"
              transparent={true}
              visible={choice === 'Number'}>
              <View style={{backgroundColor: '#B3B3B3', flex: 1}}>
                <View
                  style={{
                    backgroundColor: '#fff',
                    margin: 50,
                    padding: 40,
                    borderRadius: 10,
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      fontSize: 30,
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}>
                    Enter Phone Number
                  </Text>
                  <PhoneNumberInput
                    state={state}
                    setState={setState}
                    phoneNumber={phoneNumber}
                    isDineIn={isDineIn}
                    contactInputEditable={contactInputEditable}
                    setContactInputEditable={setContactInputEditable}
                  />
                  <Pressable
                    style={[stylespayment.buttonModel]}
                    onPress={() => setChoice('')}>
                    <Text
                      style={[
                        {
                          textAlign: 'center',
                          justifyContent: 'center',
                          color: '#fff',
                        },
                      ]}>
                      Okay
                    </Text>
                  </Pressable>
                </View>
              </View>
            </Modal>
            <TouchableOpacity
              style={stylespayment.button}
              value={choice}
              onPress={() => setChoice('Number')}>
              <Text style={{color: '#fff'}}>Number</Text>
            </TouchableOpacity>
          </View>
          {service_type === 'dine_in' && (
            // <View>
            //   <TouchableOpacity
            //     style={stylespayment.button}
            //     value={choice}
            //     onPress={() => setChoice('TablePos')}>
            //     <Text style={{color: '#fff'}}>Table Number</Text>
            //   </TouchableOpacity>
            //   {choice === 'TablePos' && (
            //     <TableNumberInput
            //       tableNo={tableNo}
            //       state={state}
            //       setState={setState}
            //       savedDineIn={savedDineIn}
            //     />
            //   )}
            // </View>
            <View>
              <Modal
                animationType="slide"
                transparent={true}
                visible={choice === 'TablePos'}>
                <View style={{backgroundColor: '#B3B3B3', flex: 1}}>
                  <View
                    style={{
                      backgroundColor: '#fff',
                      margin: 50,
                      padding: 40,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        textAlign: 'center',
                        justifyContent: 'center',
                      }}>
                      Enter Table Number
                    </Text>
                    <TableNumberInput
                      tableNo={tableNo}
                      state={state}
                      setState={setState}
                      savedDineIn={savedDineIn}
                    />
                    <Pressable
                      style={[stylespayment.buttonModel]}
                      onPress={() => setChoice('')}>
                      <Text
                        style={[
                          {
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff',
                          },
                        ]}>
                        Okay
                      </Text>
                    </Pressable>
                  </View>
                </View>
              </Modal>
              <TouchableOpacity
                style={stylespayment.button}
                value={choice}
                onPress={() => setChoice('TablePos')}>
                <Text style={{color: '#fff'}}>Table Number</Text>
              </TouchableOpacity>
            </View>
          )}
          {service_type === 'collection' && (
            // <View>
            //   <TouchableOpacity
            //     style={stylespayment.button}
            //     value={choice}
            //     onPress={() => setChoice('Name')}>
            //     <Text style={{color: '#fff'}}>Customer Name</Text>
            //   </TouchableOpacity>
            //   {choice === 'Name' && (
            //     <CustomerNameInput
            //       state={state}
            //       setState={setState}
            //       isDineIn={isDineIn}
            //       customerName={customerName}
            //       customerInputEditable={customerInputEditable}
            //       setCustomerInputEditable={setCustomerInputEditable}
            //       // setName={setName('false')}
            //     />

            //   )}
            // </View>
            <View>
              <Modal
                animationType="slide"
                transparent={true}
                visible={choice === 'Name'}>
                <View style={{backgroundColor: '#B3B3B3', flex: 1}}>
                  <View
                    style={{
                      backgroundColor: '#fff',
                      margin: 50,
                      padding: 40,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        textAlign: 'center',
                        justifyContent: 'center',
                      }}>
                      Enter Customer Name
                    </Text>
                    <CustomerNameInput
                      state={state}
                      setState={setState}
                      isDineIn={isDineIn}
                      customerName={customerName}
                      customerInputEditable={customerInputEditable}
                      setCustomerInputEditable={setCustomerInputEditable}
                      // setName={setName('false')}
                    />
                    <Pressable
                      style={[stylespayment.buttonModel]}
                      onPress={() => setChoice('')}>
                      <Text
                        style={[
                          {
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff',
                          },
                        ]}>
                        Okay
                      </Text>
                    </Pressable>
                  </View>
                </View>
              </Modal>
              <TouchableOpacity
                style={stylespayment.button}
                value={choice}
                onPress={() => setChoice('Name')}>
                <Text style={{color: '#fff'}}>Customer Name</Text>
              </TouchableOpacity>
            </View>
          )}
          {service_type === 'delivery' && (
            // <View>
            //   <TouchableOpacity
            //     style={stylespayment.button}
            //     value={choice}
            //     onPress={() => setChoice('Name')}>
            //     <Text style={{color: '#fff'}}>Customer Name</Text>
            //   </TouchableOpacity>
            //   {choice === 'Name' && (
            //     <CustomerNameInput
            //       state={state}
            //       setState={setState}
            //       isDineIn={isDineIn}
            //       customerName={customerName}
            //       customerInputEditable={customerInputEditable}
            //       setCustomerInputEditable={setCustomerInputEditable}
            //     />
            //   )}
            // </View>
            <View>
              <Modal
                animationType="slide"
                transparent={true}
                visible={choice == 'Name'}>
                <View style={{backgroundColor: '#B3B3B3', flex: 1}}>
                  <View
                    style={{
                      backgroundColor: '#fff',
                      margin: 50,
                      padding: 40,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        textAlign: 'center',
                        justifyContent: 'center',
                      }}>
                      Enter Customer Name
                    </Text>
                    <CustomerNameInput
                      state={state}
                      setState={setState}
                      isDineIn={isDineIn}
                      customerName={customerName}
                      customerInputEditable={customerInputEditable}
                      setCustomerInputEditable={setCustomerInputEditable}
                    />
                    <Pressable
                      style={[stylespayment.buttonModel]}
                      onPress={() => setChoice('')}>
                      <Text
                        style={[
                          {
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff',
                          },
                        ]}>
                        Okay
                      </Text>
                    </Pressable>
                  </View>
                </View>
              </Modal>
              <TouchableOpacity
                style={stylespayment.button}
                value={choice}
                onPress={() => setChoice('Name')}>
                <Text style={{color: '#fff'}}>Customer Name</Text>
              </TouchableOpacity>
            </View>
          )}

          {/* <OrderSummary deliveryCharge={deliveryCharge} /> */}
          {service_type !== 'dine_in' && (
            // <View style={{flexDirection: 'row'}}>
            //   <TouchableOpacity
            //     style={stylespayment.button}
            //     value={choice}
            //     onPress={() => setChoice('PaymentPos')}>
            //     <Text style={{color: '#fff'}}>Payment Options</Text>
            //   </TouchableOpacity>
            //   {choice === 'PaymentPos' && (
            //     <PaymentTypeInput
            //       paymentType={paymentType}
            //       onRadioBtnClick={onRadioBtnClick}
            //     />
            //   )}
            // </View>
            <View>
              <Modal
                animationType="slide"
                transparent={true}
                visible={choice === 'PaymentPos'}>
                <View style={{backgroundColor: '#B3B3B3', flex: 1}}>
                  <View
                    style={{
                      backgroundColor: '#fff',
                      margin: 50,
                      padding: 40,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        textAlign: 'center',
                        justifyContent: 'center',
                      }}>
                      Please Select Your payment Option
                    </Text>
                    <PaymentTypeInput
                      paymentType={paymentType}
                      onRadioBtnClick={onRadioBtnClick}
                    />
                    <Pressable
                      style={[stylespayment.buttonModel]}
                      onPress={() => setChoice('')}>
                      <Text
                        style={[
                          {
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff',
                          },
                        ]}>
                        Okay
                      </Text>
                    </Pressable>
                  </View>
                </View>
              </Modal>
              <TouchableOpacity
                style={stylespayment.button}
                value={choice}
                onPress={() => setChoice('PaymentPos')}>
                <Text style={{color: '#fff'}}>Payment Options</Text>
              </TouchableOpacity>
            </View>
          )}
          {service_type === 'delivery' && (
            // <View>
            //   <TouchableOpacity
            //     style={stylespayment.button}
            //     value={choice}
            //     onPress={() => setChoice('AddressPos')}>
            //     <Text style={{color: '#fff'}}>Address</Text>
            //   </TouchableOpacity>
            //   {choice === 'AddressPos' && (
            //     <AddressInput
            //       state={state}
            //       setState={setState}
            //       address={address}
            //       postCode={postCode}
            //       restaurantData={restaurantData}
            //       deliveryAddressInputEditable={deliveryAddressInputEditable}
            //       setDeliveryAddressInputEditable={
            //         setDeliveryAddressInputEditable
            //       }
            //     />
            //   )}
            // </View>
            <View>
              <Modal
                animationType="slide"
                transparent={true}
                visible={choice === 'AddressPos'}>
                <View style={{backgroundColor: '#B3B3B3', flex: 1}}>
                  <View
                    style={{
                      backgroundColor: '#fff',
                      margin: 50,
                      padding: 40,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: 30,
                        textAlign: 'center',
                        justifyContent: 'center',
                      }}>
                      Enter Your Address
                    </Text>
                    <AddressInput
                      state={state}
                      setState={setState}
                      address={address}
                      postCode={postCode}
                      restaurantData={restaurantData}
                      deliveryAddressInputEditable={
                        deliveryAddressInputEditable
                      }
                      setDeliveryAddressInputEditable={
                        setDeliveryAddressInputEditable
                      }
                    />
                    <Pressable
                      style={[stylespayment.buttonModel]}
                      onPress={() => setChoice('')}>
                      <Text
                        style={[
                          {
                            textAlign: 'center',
                            justifyContent: 'center',
                            color: '#fff',
                          },
                        ]}>
                        Okay
                      </Text>
                    </Pressable>
                  </View>
                </View>
              </Modal>
              <TouchableOpacity
                style={stylespayment.button}
                value={choice}
                onPress={() => setChoice('AddressPos')}>
                <Text style={{color: '#fff'}}>Address</Text>
              </TouchableOpacity>
            </View>
          )}
          <View>
            <ButtonInput
              placeOrder={placeOrder}
              service_type={service_type}
              savedDineIn={savedDineIn}
            />
          </View>
        </View>
        {/* <View style={{flex: 1}}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible2}>
            <View style={{backgroundColor: '#000', flex: 1}}>
              <View
                style={{
                  backgroundColor: '#fff',
                  margin: 50,
                  padding: 40,
                  borderRadius: 10,
                  flex: 1,
                }}>
                <Text
                  style={{
                    fontSize: 50,
                    textAlign: 'center',
                    justifyContent: 'center',
                  }}>
                  Customer Name
                </Text>
                <CustomerNameInput
                  state={state}
                  setState={setState}
                  isDineIn={isDineIn}
                  customerName={customerName}
                  customerInputEditable={customerInputEditable}
                  setCustomerInputEditable={setCustomerInputEditable}
                />
                <Pressable
                  style={[stylespayment.button]}
                  onPress={() => setModalVisible2(!modalVisible2)}>
                  <Text
                    style={[
                      stylespayment.button,
                      {
                        textAlign: 'center',
                        justifyContent: 'center',
                        color: '#fff',
                      },
                    ]}>
                    Okay
                  </Text>
                </Pressable>
              </View>
            </View>
          </Modal>
          <Pressable
            style={[styles.button, styles.buttonOpen]}
            onPress={() => setModalVisible2(true)}>
            <Text
              style={[
                stylespayment.button,
                {textAlign: 'center', justifyContent: 'center', color: '#fff'},
              ]}>
              Customer Name
            </Text>
          </Pressable>
        </View> */}
      </View>

      {isModalVisible && (
        <TableNumberModal
          isModalVisible={isModalVisible}
          setModalVisible={setModalVisible}
          state={state}
          setState={setState}
          table={restaurantData.table}
        />
      )}
    </View>
  );
}

const styles22 = StyleSheet.create({
  hi: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
});

const stylespayment = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   paddingHorizontal: 10,
  // },

  button: {
    // flex: 1,
    width: 150,
    // alignItems: 'center',
    backgroundColor: '#D2181B',
    justifyContent: 'center',
    alignItems: 'center',

    padding: 10,
    margin: 3,
    borderRadius: 5,
    maxHeight: 48,
    minHeight: 48,
  },
  buttonModel: {
    // flex: 1,
    width: '100%',
    // alignItems: 'center',
    backgroundColor: '#D2181B',
    justifyContent: 'center',
    alignItems: 'center',

    padding: 10,
    margin: 5,
    borderRadius: 5,
    maxHeight: 48,
    minHeight: 48,
    marginTop: 10,
  },
  buttonD: {
    // flex: 1,
    width: '33%',
    // alignItems: 'center',
    backgroundColor: '#656565',
    justifyContent: 'center',
    alignItems: 'center',

    padding: 10,
    margin: 3,
    borderRadius: 5,
    maxHeight: 55,
    minHeight: 55,
  },
});
