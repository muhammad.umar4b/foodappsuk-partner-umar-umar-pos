import React from 'react';
import {Text, TouchableOpacity, TouchableHighlight, View} from 'react-native';

import Loader from '../../../utilities/Loader';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

export default function FoodItemArea(props) {
  const {foodItemList, toggleAddToCart, foodItemLoading} = props;

  const {
    foodItemArea,
    detailsArea,
    detailsAreaLeft,
    detailsAreaTitle,
    detailsAreaAmount,
    itemIsActive,
    categoryViewActive,
  } = styles;
  console.log(props);

  // const {paddingTop1, paddingBottom1} = globalStyles;

  return (
    <View style={foodItemArea}>
      {foodItemLoading ? (
        <Loader />
      ) : (
        foodItemList.map((item, index) => (
          <TouchableHighlight
            activeOpacity={0.9}
            underlayColor="#00be51"
            style={detailsArea}
            key={index}
            onPress={() => toggleAddToCart(foodItemList, index)}>
            <View>
              <Text style={detailsAreaTitle}>{item.name}</Text>
              <Text style={detailsAreaAmount}>£{item.price}</Text>
            </View>
          </TouchableHighlight>
        ))
      )}
    </View>
  );
}
