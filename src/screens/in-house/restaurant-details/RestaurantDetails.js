import React, {useEffect, useState, useLayoutEffect} from 'react';
import {View, Text, ScrollView, StyleSheet, SafeAreaView} from 'react-native';
import AddToCart from '../add-to-cart/AddToCart';
import {TouchableOpacity} from 'react-native';
import Payment from '../payment/Payment';

import {connect, useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Appbar, Menu} from 'react-native-paper';

import {getWeekDay} from '../../../utilities/global';
import {getOrderDetailsAndPrint} from '../../../utilities/getOrderDetailsAndPrint';
import {
  SET_CART_LIST,
  SET_SERVICE,
  SET_SAVED_DINE_IN,
  SET_ORDER_DETAILS,
} from '../../../store/types';

import {
  getRestaurantCategoryById,
  getRestaurantInfo,
  getRestaurantItemsByCategoryId,
} from '../../../store/actions/restaurant';
// import {
//   continueButton,
//   continueText,
// } from '../../../assets/styles/allOrderStyles';
// import {marginRight4} from '../../../assets/styles/globalStyles';
// Component
import BasketArea from './BasketArea';
import Settings from '../../home/Settings';
import CategoryArea from './CategoryArea';
import Loader from '../../../utilities/Loader';
import ServiceTypeArea from './ServiceTypeArea';
import AddToCartModal from './add-to-cart-modal/AddToCartModal';
import SpecialOfferModal from './special-offer-modal/SpecialOfferModal';
import BusinessProfile from '../../business-profile/BusinessProfile';
// import {LOGOUT} from '../../auth/Login';
// import {useDispatch} from 'react-redux';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';
import {sample} from 'lodash';

const RestaurantDetails = ({navigation, route, ...props}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [modalState, setModalState] = useState(null);
  const [categoryList, setCategoryList] = useState([]);
  const [restaurantInfo, setRestaurantInfo] = useState(null);
  const [restaurantInfoLoading, setRestaurantInfoLoading] = useState(true);
  const [serviceListArea, setServiceListArea] = useState([
    {
      key: 'dine_in',
      name: 'Dine In',
      isActive: true,
    },
    {
      key: 'collection',
      name: 'Collection',
      isActive: false,
    },
    {
      key: 'delivery',
      name: 'Delivery',
      isActive: false,
    },
  ]);
  const [isViewBasket, setIsViewBasket] = useState(false);
  const [state, setState] = useState({
    count: 1,
    isAdd: false,
  });
  const [isRestaurantOn, setIsRestaurantOn] = useState(true);
  const [isSpecialOfferModalVisible, setIsSpecialOfferModalVisible] =
    useState(false);
  const [specialOfferModalState, setSpecialOfferModalState] = useState(null);
  const [activeCategory, setActiveCategory] = useState(null);
  const [foodItemLoading, setFoodItemLoading] = useState(false);
  const [foodItemList, setFoodItemList] = useState([]);
  const [categoryListLoading, setCategoryListLoading] = useState(false);
  const [visible, setVisible] = useState(false);

  const dispatch = useDispatch();
  const logOut = async () => {
    await AsyncStorage.clear();
    dispatch({type: 'LOGOUT', logged: false});
    showToastWithGravityAndOffset('Logged out successfully!');
  };

  const {
    restaurant: {service_type, cartList},
  } = useSelector(state => state);

  // console.log(`hello  ${categories}`);

  const {count, isAdd} = state;

  const {container, paddingHorizontal0, f16, textWhite} = globalStyles;

  const {menuBarServiceArea, container2} = styles;
  // const [openMenu, setOpenMenu] = useState(false);

  useEffect(() => {
    const activeService = serviceListArea.find(item => item.isActive && item);
    navigation.setOptions({
      title: (
        <ServiceTypeArea
          // style={{marginLeft: 100}}
          serviceListArea={serviceListArea}
          updateServiceType={updateServiceType}
        />
      ),
      headerRight: () => {
        // <View>
        //   <TouchableOpacity
        //     style={styles1.continueButton}
        //     onPress={() => navigation.push('InHouseOrder')}>
        //     <Text style={styles1.continueText}>Orders</Text>
        //   </TouchableOpacity>
        // </View>;

        // <ScrollView
        //   style={styles1.container}
        //   showsVerticalScrollIndicator={false}>
        //   <View style={styles1.items}>
        // <TouchableOpacity
        //   style={styles1.continueButton}
        //   onPress={() => navigation.push('InHouseOrder')}>
        //   <Text style={styles1.continueText}>Orders</Text>
        // </TouchableOpacity>
        // let mvisible = false;
        // const [isMVisible, setIsMVisible] = useState(false);
        const openMenu = () => {
          console.log('hello world1');
          setVisible(true);
          // console.log(visible);
          // mvisible = !mvisible;
          // visible = true;
          setTimeout(() => {
            console.log('===>', visible);
          }, 200);
        };
        const closeMenu = () => {
          // visible = false;
          setVisible(pre => false);
          // mvisible = !mvisible;
        };

        return (
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
              <Appbar.Action icon="menu" color="#D2181B" onPress={openMenu} />
            }>
            <Menu.Item
              onPress={() => navigation.navigate('BusinessProfile')}
              title="Business Profile"
            />
            <Menu.Item
              onPress={() => navigation.navigate('MyOrder')}
              title="My Orders"
            />
            <Menu.Item
              onPress={() => navigation.navigate('InHouseOrder')}
              title="In-House"
            />
            <Menu.Item
              onPress={() => navigation.navigate('MyBookings')}
              title="My Bookings"
            />
            <Menu.Item
              onPress={() => navigation.navigate('SalesReport')}
              title="Sales Report"
            />
            <Menu.Item
              onPress={() => navigation.navigate('Invoice')}
              title="Invoice"
            />
            <Menu.Item
              onPress={() => navigation.navigate('Notifications')}
              title="Notifications"
            />
            <Menu.Item
              onPress={() => navigation.navigate('TermsAndConditions')}
              title="Terms & Conditions"
            />
            <Menu.Item
              onPress={() => navigation.navigate('Printer')}
              title="Printer"
            />
            <Menu.Item onPress={() => logOut()} title="Sign Out" />
          </Menu>
        );
      },
    });
  }, [visible, navigation, serviceListArea, restaurantInfo]);

  useEffect(() => {
    getRestaurantInfo(dispatch).then(res => {
      console.log('RESTAURANT INFO: ', !!res);

      const {
        restaurantTiming: {
          restaurantTiming: {available},
        },
      } = res;

      const weekDay = getWeekDay();
      const restaurantTime = available.find(item => item['day'] === weekDay);

      setRestaurantInfo(res);
      setIsRestaurantOn(restaurantTime ? restaurantTime.isOpen : false);
      setRestaurantInfoLoading(false);
    });
  }, []);

  useEffect(() => {
    if (route.params && route.params.orderDetailsId) {
      getOrderDetailsAndPrint(route.params.orderDetailsId).then(res => {
        console.log('ORDER DETAILS: ', !!res);

        const {cartItems, orderType} = res;

        if (orderType === 'Dine In') {
          const updateServiceListArea = serviceListArea.map(item => {
            item.isActive = item.key === 'dine_in';
            return item;
          });

          setServiceListArea(updateServiceListArea);
          dispatch({type: SET_SERVICE, service_type: 'dine_in'});
        }

        dispatch({type: SET_CART_LIST, payload: JSON.parse(cartItems)});
        dispatch({type: SET_SAVED_DINE_IN, payload: true});
        dispatch({type: SET_ORDER_DETAILS, payload: res});
      });
    } else {
      const updateServiceListArea = serviceListArea.map(item => {
        item.isActive = item.key === service_type;
        return item;
      });

      setServiceListArea(updateServiceListArea);

      dispatch({type: SET_SERVICE, service_type});
      dispatch({type: SET_CART_LIST, payload: null});
      dispatch({type: SET_SAVED_DINE_IN, payload: false});
      dispatch({type: SET_ORDER_DETAILS, payload: null});
    }
  }, [route]);

  // useEffect(() => {
  //   console.log('categooooooo', props.categories);
  //   // console.log(getCategories);
  // }, [props.categories]);

  useEffect(() => {
    if (restaurantInfo) {
      setCategoryListLoading(true);
      getRestaurantCategoryById(restaurantInfo.uniId).then(res => {
        // props.createCategories(res);

        const updateCategoryList = res.categories.map(item => {
          item.isActive = false;
          return item;
        });
        setCategoryList(updateCategoryList);
        setCategoryListLoading(false);
      });
    }
  }, [restaurantInfo]);

  useEffect(() => {
    if (modalState && isViewBasket && isAdd) {
      const {_id, name, options, basePrice, restaurant} = modalState;

      const foodItem = {
        food: _id,
        name,
        basePrice,
        options,
        optionsPrice: 0,
        price: parseFloat(parseFloat(basePrice * count).toFixed(2)),
        quantity: count,
        isAdded: false,
        uniqueId:
          name.substring(0, 3).toUpperCase() +
          Math.floor(Math.random() * 1000) +
          1,
      };

      let updateCartList;

      if (cartList) {
        updateCartList = {...cartList};
      } else {
        updateCartList = {
          note: '',
          customer: '',
          restaurant,
          count: 0,
          subTotal: 0,
          foodItems: [],
        };
      }

      updateCartList.foodItems.push(foodItem);

      let subTotal = 0;
      updateCartList.foodItems.forEach(item => {
        subTotal = parseFloat(parseFloat(subTotal + item.price).toFixed(2));
      });

      updateCartList.subTotal = subTotal;
      updateCartList.count = updateCartList.foodItems.length;

      dispatch({type: SET_CART_LIST, payload: updateCartList});
    }
  }, [count, modalState, isViewBasket, isAdd]);

  useEffect(() => {
    setIsViewBasket(!!cartList);
  }, [cartList]);

  useEffect(() => {
    if (activeCategory) {
      setFoodItemLoading(true);
      getRestaurantItemsByCategoryId(activeCategory).then(res => {
        console.log('FOOD CATEGORY: ', !!res);

        if (res) {
          setFoodItemList(res);
          setFoodItemLoading(false);
        }
      });
    }
  }, [activeCategory]);

  const updateServiceType = key => {
    const updateServiceListArea = serviceListArea.map((item, index) => {
      item.isActive = key === index;
      return item;
    });

    const activeService = updateServiceListArea.find(
      item => item.isActive === true,
    );
    dispatch({type: SET_SERVICE, service_type: activeService.key});
    setServiceListArea(updateServiceListArea);

    if (
      activeService.key === 'dine_in' &&
      route.params &&
      route.params.orderDetailsId
    ) {
      dispatch({type: SET_SAVED_DINE_IN, payload: true});
    } else {
      dispatch({type: SET_SAVED_DINE_IN, payload: false});
    }
  };

  const toggleCategory = activeCategory => {
    const updateCategoryList = categoryList.map(item => {
      if (item._id === activeCategory) {
        item.isActive = !item.isActive;
      } else item.isActive = false;
      // console.log(item);
      return item;
    });

    setCategoryList(updateCategoryList);
    setActiveCategory(activeCategory);
  };

  const toggleAddToCart = (foodItemList, index) => {
    const foodItem = foodItemList.find((item, key) => key === index);

    setState({
      count: 1,
      isAdd: true,
    });
    console.log(foodItem.options.length);

    if (foodItem.options.length > 0) {
      setSpecialOfferModalState(foodItem);
      setIsSpecialOfferModalVisible(!isSpecialOfferModalVisible);
    } else {
      setModalState(foodItem);
      setTimeout(() => {
        onClickAddToCart();
      }, 200);
      // setModalState(foodItem);
      // setModalVisible(!isModalVisible);
    }
  };

  const onClickAddToCart = () => {
    if (count >= 1) {
      const updateState = {...state};
      updateState.count = 1;
      updateState.isAdd = true;
      setState(updateState);
    }

    // setModalVisible(!isModalVisible);
    setIsViewBasket(true);
  };

  const renderRestaurantInfo = restaurantInfo && (
    <>
      <ScrollView showsVerticalScrollIndicator={true}>
        {/* <ServiceTypeArea
          serviceListArea={serviceListArea}
          updateServiceType={updateServiceType}
        /> */}
        <View style={styles.container2}>
          <View
            style={[
              styles.flex1half,
              {width: '30%', height: 'auto'},
              {marginRight: 10},
            ]}>
            {foodItemList && (
              <AddToCart
                navigation={navigation}
                totalPrice={cartList ? cartList.subTotal : 0}
                isRestaurantOn={isRestaurantOn}
              />
            )}
          </View>
          <View style={[styles.flex11]}>
            <CategoryArea
              route={route}
              navigation={navigation}
              categoryList={categoryList}
              activeCategory={activeCategory}
              toggleCategory={toggleCategory}
              foodItemLoading={foodItemLoading}
              foodItemList={foodItemList}
              toggleAddToCart={toggleAddToCart}
              isViewBasket={isViewBasket}
              categoryListLoading={categoryListLoading}
            />
          </View>
        </View>
      </ScrollView>

      <AddToCartModal
        state={state}
        setState={setState}
        modalState={modalState}
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
        setIsViewBasket={setIsViewBasket}
      />

      <SpecialOfferModal
        cartList={cartList}
        dispatch={dispatch}
        specialOfferModalState={specialOfferModalState}
        isSpecialOfferModalVisible={isSpecialOfferModalVisible}
        setIsSpecialOfferModalVisible={setIsSpecialOfferModalVisible}
      />
    </>
  );

  return restaurantInfoLoading ? <Loader /> : renderRestaurantInfo;
};

const styles1 = StyleSheet.create({
  container: {flex: 1, height: 10, alignSelf: 'flex-start'},
  items: {
    alignSelf: 'flex-start',
    flexWrap: 'wrap',
    width: 100,
    flexDirection: 'row',
  },
  continueButton: {
    backgroundColor: '#D2181B',
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderRadius: 8,
    marginRight: 10,
  },
  continueText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
});
const getCategories = dispatch => {
  return {
    createCategories: sample => dispatch(createCategories(sample)),
  };
};
const mapCategoriesStateToProps = state => {
  return {
    categories: state.sample,
  };
};

export default connect(
  getCategories,
  mapCategoriesStateToProps,
)(RestaurantDetails);
