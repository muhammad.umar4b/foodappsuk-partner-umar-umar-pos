import React, {useState} from 'react';
import {ScrollView, Text, View} from 'react-native';

import Modal from 'react-native-modal';

import styles from '../styles';
import globalStyles from '../../../../assets/styles/globalStyles';

import Body from './Body';
import Header from './Header';
import Footer from './Footer';

const SpecialOfferModal = props => {
  const [tableData, setTableData] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);

  const {
    cartList,
    dispatch,
    specialOfferModalState,
    isSpecialOfferModalVisible,
    setIsSpecialOfferModalVisible,
  } = props;

  const {modalBody} = styles;

  const {modalView} = globalStyles;

  return (
    <Modal
      isVisible={isSpecialOfferModalVisible}
      animationIn="fadeIn"
      animationOut="fadeOut"
      animationInTiming={500}
      animationOutTiming={500}
      style={modalView}>
      {specialOfferModalState ? (
        <View
          style={{
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ScrollView style={{width: '60%'}}>
            <Header
              specialOfferModalState={specialOfferModalState}
              isSpecialOfferModalVisible={isSpecialOfferModalVisible}
              setIsSpecialOfferModalVisible={setIsSpecialOfferModalVisible}
            />
            <View style={modalBody}>
              <Body
                tableData={tableData}
                setTableData={setTableData}
                totalPrice={totalPrice}
                setTotalPrice={setTotalPrice}
                specialOfferModalState={specialOfferModalState}
              />
              <Footer
                cartList={cartList}
                dispatch={dispatch}
                tableData={tableData}
                totalPrice={totalPrice}
                specialOfferModalState={specialOfferModalState}
                isSpecialOfferModalVisible={isSpecialOfferModalVisible}
                setIsSpecialOfferModalVisible={setIsSpecialOfferModalVisible}
              />
            </View>
          </ScrollView>
        </View>
      ) : (
        <View>
          <Text>Item not found!</Text>
        </View>
      )}
    </Modal>
  );
};

export default SpecialOfferModal;
