import React from "react";

import SingleItem from "./SingleItem";
import Divider from "./Divider";

const Body = ({ invoiceData }) => {
    const {
        grossTotal,
        cashCollected,
        faCharges,
        serviceCharge,
        riderCharge,
        bookingCharge,
        bankCharge,
        total,
        orderCount: {
            bookings,
            dineIn,
            homeDelivery,
            collection,
        },
    } = invoiceData;

    return (
        <>
            <SingleItem label={"Booking Orders"} price={bookings} />
            <SingleItem label={"Collection Orders"} price={collection} />
            <SingleItem label={"Dine in Orders"} price={dineIn} />
            <SingleItem label={"Home Delivery Orders"} price={homeDelivery} />
            <Divider />
            <SingleItem label={"Gross Income"} price={grossTotal} />
            <SingleItem label={"Cash Collected"} price={cashCollected} />
            <SingleItem label={"FA Charge"} price={faCharges} />
            <SingleItem label={"Service Charge"} price={serviceCharge} />
            <SingleItem label={"Rider Charge"} price={riderCharge} />
            <SingleItem label={"Booking Charge"} price={bookingCharge} />
            <SingleItem label={"Bank Charge"} price={bankCharge} />
            <Divider />
            <SingleItem label={"Total"} price={total} />
        </>
    );
};


export default Body;
