import React, {useEffect, useState} from 'react';
import {ScrollView} from 'react-native';

import axios from 'axios';

import styles from '../styles';

import {apiBaseUrl} from '../../../config/index-example';

// Component
import Loader from '../../../utilities/Loader';
import RenderData from './RenderData';

const AllBookings = ({restaurantId, setTotalAllBooking}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getAllBookings = async restaurantId => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/get-booking-all-orders/${restaurantId}`,
      );
      if (response.data.data) {
        setData(response.data.data);
        setTotalAllBooking(response.data.data.length);
        setIsLoading(false);
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  useEffect(() => {
    getAllBookings(restaurantId).then(res =>
      console.log('ALL BOOKINGS: ', res),
    );
  }, [restaurantId]);

  const {bookingsArea} = styles;

  return (
    <ScrollView style={bookingsArea}>
      {isLoading ? <Loader /> : <RenderData data={data} />}
    </ScrollView>
  );
};

export default AllBookings;
