import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "#e4e4e4",
        },
        tabArea: {
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: hp("1%"),
            marginHorizontal: wp("5%"),
        },
        tabButton: {
            paddingVertical: hp("2%"),
            backgroundColor: "#fff",
            width: wp("45%"),
        },
        tabButtonText: {
            textAlign: "center",
            color: "#000",
        },
        activeTabButton: {
            borderBottomColor: "#d2181b",
            borderBottomWidth: 2,
        },
        activeTabButtonText: {
            color: "#d2181b",
        },
        bookingsArea: {
            marginTop: hp("2%"),
        },
        bookingsItem: {
            backgroundColor: "#fff",
            paddingHorizontal: wp("3%"),
            paddingVertical: hp("1%"),
            borderRadius: 8,
            marginBottom: hp("2%"),
            marginHorizontal: wp("5%"),
            elevation: 5,
        },
        bookingsHeader: {
            flexDirection: "row",
            justifyContent: "space-between",
        },
        bookingsHeaderText: {
            fontWeight: "600",
            fontSize: 18,
            paddingBottom: hp("1%"),
        },
        pendingBookingTextArea: {
            flexDirection: "row",
            justifyContent: "space-between",
        },
        pendingBookingLabel: {
            lineHeight: 22,
        },
        pendingButtonArea: {
            borderTopColor: "#b4b4b4",
            borderTopWidth: 1,
            marginTop: hp("1%"),
            paddingTop: hp("1%"),
            flexDirection: "row",
            justifyContent: "center",
        },
        acceptButton: {
            backgroundColor: "#635d5d",
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("1%"),
            marginRight: wp("2%"),
            borderRadius: 8,
        },
        acceptButtonText: {
            color: "#fff",
        },
        declineButton: {
            backgroundColor: "#d2181b",
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("1%"),
            marginRight: wp("2%"),
            borderRadius: 8,
        },
        declineButtonText: {
            color: "#fff",
        },
        allBookingText: {
            lineHeight: 18,
        },
    },
);

export default styles;
