import React from 'react';
import {Text} from 'react-native';

import axios from 'axios';

import globalStyles from '../../../assets/styles/globalStyles';

import {apiBaseUrl} from '../../../config/index-example';
import {printContent} from '../../../utilities/order-print';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

import SingleItem from './SingleItem';

const RenderData = props => {
  const {data, navigation, getPendingOrders, restaurantId, restaurantData} =
    props;

  const {paddingTop1} = globalStyles;

  const setStatusBgColor = orderStatus => {
    if (orderStatus === 'Pending') return globalStyles.bgWarning;
  };

  const getOrderDetails = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/get-single-order/${id}`,
      );
      if (response.data) {
        return response.data.data;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const acceptOrder = async id => {
    try {
      const response = await axios.put(
        `${apiBaseUrl}order/restaurant-accept-order/${id}`,
      );
      if (response.data) {
        showToastWithGravityAndOffset(response.data.message);
        getOrderDetails(id).then(res => printContent(res, restaurantData));
        getPendingOrders(restaurantId).then(res =>
          console.log('PENDING ORDERS: ', res),
        );
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const declineOrder = async id => {
    try {
      const response = await axios.put(
        `${apiBaseUrl}order/restaurant-cancel-order/${id}`,
      );
      if (response.data) {
        showToastWithGravityAndOffset(response.data.message);
        getPendingOrders(restaurantId).then(res =>
          console.log('PENDING ORDERS: ', res),
        );
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  return data.length > 0 ? (
    data.map((item, index) => (
      <SingleItem
        navigation={navigation}
        item={item}
        key={index}
        setStatusBgColor={setStatusBgColor}
        acceptOrder={acceptOrder}
        declineOrder={declineOrder}
      />
    ))
  ) : (
    <Text style={paddingTop1}>No Order Yet!</Text>
  );
};

export default RenderData;
