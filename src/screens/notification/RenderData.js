import React from "react";
import { View } from "react-native";

import globalStyles from "../../assets/styles/globalStyles";

import SingleItem from "./SingleItem";

const RenderData = ({ data, deleteNotification }) => {
    const {
        paddingTop2,
    } = globalStyles;

    return (
        <View style={paddingTop2}>
            {data.map((item, index) => (
                <SingleItem
                    item={item}
                    key={index}
                    deleteNotification={deleteNotification}
                />
            ))}
        </View>
    );
};

export default RenderData;
