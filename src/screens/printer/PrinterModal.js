import React from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";

import Modal from "react-native-modal";

import styles from "./styles";

const PrinterModal = ({ isModalVisible, setModalVisible, state, setState }) => {
    const {
        printers,
        activePrinter,
    } = state;

    const {
        modalView,
        modalBody,
        modalFooter,
        monthArea,
        monthAreaContent,
        monthAreaContentBlock,
        monthAreaContentText,
        monthAreaContentTextActive,
        monthAreaHeaderText,
        closeButton,
        continueText,
    } = styles;

    return (
        <Modal
            isVisible={isModalVisible}
            style={modalView}>
            <View>
                <View style={modalBody}>
                    <View style={monthArea}>
                        <Text style={monthAreaHeaderText}>Select Printer</Text>
                        <ScrollView style={monthAreaContent}>
                            {printers.map((item, index) => (
                                <TouchableOpacity
                                    style={monthAreaContentBlock}
                                    key={index}
                                    onPress={() => {
                                        const updateState = { ...state };
                                        updateState.activePrinter = item.inner_mac_address;
                                        updateState.activePrinterInfo = item;
                                        setState(updateState);
                                    }}
                                >
                                    <Text
                                        style={[monthAreaContentText, activePrinter === item.inner_mac_address && monthAreaContentTextActive]}>
                                        {item.device_name}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </View>
                </View>
                <View style={modalFooter}>
                    <TouchableOpacity
                        style={closeButton}
                        onPress={() => setModalVisible(!isModalVisible)}
                    >
                        <Text style={continueText}>Ok</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};

export default PrinterModal;
