import React, { useEffect, useState } from "react";

import BluetoothStateManager from "react-native-bluetooth-state-manager";

import { useSelector, useDispatch } from "react-redux";
import { BLEPrinter } from "react-native-thermal-receipt-printer";

import PrinterModal from "./PrinterModal";
import PrinterHeader from "./PrinterHeader";
import PrinterInput from "./PrinterInput";

const PrinterScreen = ({ navigation }) => {
    const [isModalVisible, setModalVisible] = useState(false);
    const [state, setState] = useState({
        printers: [],
        activePrinter: "",
        activePrinterInfo: null,
        connectionType: "bluetooth",
        editable: false,
    });

    const dispatch = useDispatch();
    const globalState = useSelector(state => state);

    const {
        auth: {
            activeBluetoothPrinter,
            printerType,
        },
    } = globalState;

    const {
        activePrinter,
        connectionType,
        activePrinterInfo,
    } = state;

    const savePrinterSettings = () => {
        dispatch({ type: "SET_ACTIVE_PRINTER_INFO", printer: activePrinterInfo });
        dispatch({ type: "SET_PRINTER", printer: activePrinter });
        dispatch({ type: "SET_PRINTER_TYPE", printer: connectionType });
        setState({ ...state, editable: false });
    };

    const printContent = async () => {
        BluetoothStateManager.enable().then(result => {
            BLEPrinter.init().then(() => {
                BLEPrinter.getDeviceList().then(value => {
                    console.log(value, 'value');
                    const updateState = { ...state };
                    updateState.printers = value;
                    updateState.activePrinterInfo = globalState["auth"]["activePrinterInfo"] ? globalState["auth"]["activePrinterInfo"] : null;
                    updateState.connectionType = printerType;
                    updateState.activePrinter = activeBluetoothPrinter;
                    setState(updateState);
                }).catch(error => console.log(error, "get device list error"));
            }).catch(error => console.log(error, "blep printer error"));
        }).catch(error => console.log(error, "state manager error"));
        return true;
    };

    useEffect(() => {
        printContent().then(res => console.log(res, "printContent() res"));
    }, []);

    return (
        <>
            <PrinterHeader
                navigation={navigation}
                state={state}
                setState={setState}
                savePrinterSettings={savePrinterSettings}
                printContent={printContent}
            />
            <PrinterInput
                state={state}
                isModalVisible={isModalVisible}
                setModalVisible={setModalVisible}
            />
            <PrinterModal
                isModalVisible={isModalVisible}
                setModalVisible={setModalVisible}
                state={state}
                setState={setState}
            />
        </>
    );
};

export default PrinterScreen;
