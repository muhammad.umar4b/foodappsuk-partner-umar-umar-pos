import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    editButtonArea: {
        backgroundColor: "#d2181b",
        height: hp("4%"),
        paddingLeft: wp("4%"),
        paddingRight: wp("4%"),
        borderRadius: 8,
    },
    editButtonText: {
        fontSize: 14,
        color: "white",
    },
    inputLabel: {
        color: "#555555",
    },
    inputField: {
        paddingVertical: hp("1%"),
        paddingLeft: wp("4%"),
        borderColor: "#d9d3d3",
        borderWidth: 1,
        borderRadius: 8,
        color: "#000",
    },
    selectionInputArea: {
        borderColor: "#d9d3d3",
        borderWidth: 1,
        borderRadius: 8,
    },
    modalView: {
        marginVertical: hp("0%"),
        marginHorizontal: wp("10%"),
        justifyContent: "center",
    },
    modalBody: {
        backgroundColor: "#fff",
        paddingVertical: hp("2%"),
        paddingHorizontal: wp("5%"),
        flexDirection: "row",
        justifyContent: "center",
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    modalFooter: {
        paddingVertical: hp("2%"),
        backgroundColor: "#fff",
        flexDirection: "row",
        justifyContent: "center",
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
    },
    closeButton: {
        backgroundColor: "#D2181B",
        paddingVertical: hp("1%"),
        paddingHorizontal: wp("5%"),
        borderRadius: 8,
    },
    monthArea: {
        width: wp("35%"),
    },
    monthAreaHeaderText: {
        fontSize: 18,
        paddingBottom: wp("2%"),
        paddingRight: wp("5%"),
    },
    monthAreaContent: {
        height: 100,
    },
    monthAreaContentBlock: {
        width: wp("30%"),
    },
    monthAreaContentText: {
        fontSize: 16,
        color: "#000",
        paddingVertical: hp("0.5%"),
        borderRadius: 8,
        paddingLeft: wp("2%"),
    },
    monthAreaContentTextActive: {
        backgroundColor: "#D2181B",
        color: "#fff",
        paddingLeft: wp("2%"),
    },
    continueText: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "700",
        textAlign: "center",
    },
});

export default styles;
