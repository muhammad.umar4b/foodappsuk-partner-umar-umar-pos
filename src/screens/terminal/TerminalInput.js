import React from 'react';
import {Pressable, SafeAreaView, Text, TextInput, View} from 'react-native';

import styles from './styles';
import globalStyles from '../../assets/styles/globalStyles';

const TerminalInput = ({state, isModalVisible, setModalVisible}) => {
  const {connectionType, activeTerminalInfo, editable} = state;

  const {paddingTop3, paddingBottom1, container} = globalStyles;

  const {inputLabel, inputField} = styles;

  return (
    <SafeAreaView style={container}>
      <View style={paddingTop3}>
        <Text style={[paddingBottom1, inputLabel]}>Connection Type</Text>
        <TextInput
          value={connectionType === 'bluetooth' ? 'Bluetooth' : connectionType}
          style={inputField}
          keyboardType={'default'}
          editable={false}
          placeholderTextColor="#000"
        />
      </View>
      <View style={paddingTop3}>
        <Text style={[paddingBottom1, inputLabel]}>Terminal Name</Text>
        <Pressable onPress={() => editable && setModalVisible(!isModalVisible)}>
          <TextInput
            value={activeTerminalInfo ? activeTerminalInfo.device_name : ''}
            placeholder={'Terminal Name'}
            style={inputField}
            editable={false}
            keyboardType={'default'}
            placeholderTextColor="#000"
          />
        </Pressable>
      </View>
    </SafeAreaView>
  );
};

export default TerminalInput;
