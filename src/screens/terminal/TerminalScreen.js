import React, {useEffect, useState} from 'react';

import BluetoothStateManager from 'react-native-bluetooth-state-manager';

import {useSelector, useDispatch} from 'react-redux';
import {BLEPrinter} from 'react-native-thermal-receipt-printer';

import TerminalModal from './TerminalModal';
import TerminalHeader from './TerminalHeader';
import TerminalInput from './TerminalInput';

const TerminalScreen = ({navigation}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [state, setState] = useState({
    terminals: [],
    activeTerminal: '',
    activeTerminalInfo: null,
    connectionType: 'bluetooth',
    editable: false,
  });

  const dispatch = useDispatch();
  const globalState = useSelector(state => state);

  const {
    auth: {activeBluetoothTerminal, printerType},
  } = globalState;

  const {activeTerminal, connectionType, activeTerminalInfo} = state;

  const saveTerminalSettings = () => {
    dispatch({type: 'SET_ACTIVE_PRINTER_INFO', printer: activeTerminalInfo});
    dispatch({type: 'SET_PRINTER', printer: activeTerminal});
    dispatch({type: 'SET_PRINTER_TYPE', printer: connectionType});
    setState({...state, editable: false});
  };

  const printContent = async () => {
    BluetoothStateManager.enable()
      .then(result => {
        BLEPrinter.init()
          .then(() => {
            BLEPrinter.getDeviceList()
              .then(value => {
                console.log(value, 'value');
                const updateState = {...state};
                updateState.terminals = value;
                updateState.activeTerminalInfo = globalState['auth'][
                  'activeTerminalInfo'
                ]
                  ? globalState['auth']['activeTerminalInfo']
                  : null;
                updateState.connectionType = printerType;
                updateState.activeTerminal = activeBluetoothTerminal;
                setState(updateState);
              })
              .catch(error => console.log(error, 'get device list error'));
          })
          .catch(error => console.log(error, 'blep printer error'));
      })
      .catch(error => console.log(error, 'state manager error'));
    return true;
  };

  useEffect(() => {
    printContent().then(res => console.log(res, 'printContent() res'));
  }, []);

  return (
    <>
      <TerminalHeader
        navigation={navigation}
        state={state}
        setState={setState}
        saveTerminalSettings={saveTerminalSettings}
        printContent={printContent}
      />
      <TerminalInput
        state={state}
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
      />
      <TerminalModal
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
        state={state}
        setState={setState}
      />
    </>
  );
};

export default TerminalScreen;
