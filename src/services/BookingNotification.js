import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import {apiBaseUrl} from '../config/index-example';
import {showToastWithGravityAndOffset} from '../shared-components/ToastMessage';
import {useSelector} from 'react-redux';
import SoundPlayer from 'react-native-sound-player';

const BookingNotification = ({navigation, route}) => {
  const {data, title, message} = route.params;
  const [bookingInfo, setBookingInfo] = useState(null);
  const state = useSelector(state => state);

  const {
    auth: {userId},
  } = state;

  useEffect(() => {
    if (Object.keys(data).length > 0 && data['data_string']) {
      setBookingInfo(JSON.parse(data['data_string']));
    } else {
      navigation.navigate('Home');
    }
  }, [route.params]);

  const acceptOrder = async () => {
    try {
      const payload = {
        isAccepted: true,
        ownerId: userId,
      };

      const response = await axios.put(
        `${apiBaseUrl}restaurant/update-booking/${bookingInfo['_id']}`,
        payload,
      );
      if (response.data) {
        try {
          SoundPlayer.stop('nuclearalarm', 'mp3');
        } catch (e) {
          console.log(`cannot play the sound file`, e);
        }

        console.log(response.data, 40);

        showToastWithGravityAndOffset('Booking accepted successfully!');
        navigation.navigate('MyBookings', {activeTab: 0});
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const declineOrder = async () => {
    try {
      const response = await axios.delete(
        `${apiBaseUrl}restaurant/remove-booking/${bookingInfo['_id']}`,
      );
      console.log(response.data, 47);
      if (response.data) {
        try {
          SoundPlayer.stop('nuclearalarm', 'mp3');
        } catch (e) {
          console.log(`cannot play the sound file`, e);
        }

        showToastWithGravityAndOffset('Booking declined successfully!');
        navigation.navigate('MyBookings', {activeTab: 0});
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  return bookingInfo ? (
    <View style={styles.container}>
      <View>
        <Text style={styles.restaurantName}>{title ? title : 'N/A!'}</Text>
        <Text>{message ? message : 'N/A'}</Text>
      </View>
      <View>
        <View style={styles.acceptButtonArea}>
          <TouchableOpacity style={[styles.acceptButton]} onPress={acceptOrder}>
            <Text style={styles.acceptText}>Accept</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.declineButtonArea}>
          <TouchableOpacity
            style={[styles.declineButton]}
            onPress={declineOrder}>
            <Text style={styles.declineText}>Decline</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  ) : (
    <View style={styles.notFoundArea}>
      <Text>Booking Information Not found!</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    elevation: 3,
    marginHorizontal: wp('5%'),
    marginVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('2%'),
    borderRadius: 10,
  },
  notFoundArea: {
    backgroundColor: '#fff',
    elevation: 3,
    marginHorizontal: wp('5%'),
    marginVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('2%'),
    borderRadius: 10,
  },
  restaurantName: {
    fontSize: 18,
  },
  acceptButtonArea: {
    marginTop: hp('2%'),
  },
  acceptButton: {
    backgroundColor: '#1fbf21',
    borderRadius: 8,
    height: hp('41%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  acceptText: {
    color: '#fff',
    fontSize: 30,
    fontWeight: '700',
  },
  declineButtonArea: {
    marginTop: hp('2%'),
  },
  declineButton: {
    backgroundColor: '#D2181B',
    borderRadius: 8,
    height: hp('41%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  declineText: {
    color: '#fff',
    fontSize: 30,
    fontWeight: '700',
  },
});

export default BookingNotification;
