import PushNotification from "react-native-push-notification";

export const LocalNotification = (data) => {
    PushNotification.localNotification(
        {
            channelId      : "partner_app_channel",
            title          : data ? data.title : "Test Title",
            message        : data ? data.message : "Test Message",
            vibrate        : true,
            vibration      : 300,
            playSound      : true,
            soundName      : "nuclearalarm.mp3",
            priority       : "high",
            importance     : "high",
            visibility     : "public",
            largeIcon      : "ic_launcher",
            smallIcon      : "ic_notification",
            userInteraction: false
        }
    );
};


