import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import styles from "./styles";

const NotificationBody = (props) => {
    const {
        acceptOrder,
        declineOrder,
        title,
        message,
    } = props;

    const {
        container,
        restaurantName,
        acceptButtonArea,
        acceptButton,
        acceptText,
        declineButtonArea,
        declineButton,
        declineText,
    } = styles;

    return (
        <View style={container}>
            <View>
                <Text style={restaurantName}>{title ? title : "N/A!"}</Text>
                <Text>{message ? message : "N/A"}</Text>
            </View>
            <View>
                <View style={acceptButtonArea}>
                    <TouchableOpacity style={[acceptButton]} onPress={acceptOrder}>
                        <Text style={acceptText}>Accept</Text>
                    </TouchableOpacity>
                </View>
                <View style={declineButtonArea}>
                    <TouchableOpacity style={[declineButton]} onPress={declineOrder}>
                        <Text style={declineText}>Decline</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default NotificationBody;
