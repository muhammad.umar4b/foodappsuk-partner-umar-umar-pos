import axios from 'axios';

import {apiBaseUrl} from '../../config/index-example';
import {SET_RESTAURANT_DATA} from '../types';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';
import {SET_ALL_CATEGORIES} from '../types';

const getServiceCharge = async () => {
  try {
    const response = await axios.get(
      `${apiBaseUrl}service-charge/get-service-charge`,
    );
    if (response.data) {
      const {serviceCharge} = response.data.data;
      return serviceCharge;
    }
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
    }
  }
};

export const getRestaurantInfo = async dispatch => {
  try {
    const response = await axios.get(
      `${apiBaseUrl}restaurant/get-own-restaurant-info`,
    );

    if (response.data) {
      if (response.data.success) {
        getServiceCharge().then(res => {
          console.log('SERVICE CHARGE: ', true);
          if (res) {
            response.data.data.serviceCharge = res;
            setRestaurantData(response.data.data, dispatch);
          }
        });
      }

      return response.data.data;
    }

    return null;
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return null;
  }
};

const setRestaurantData = (data, dispatch) => {
  const {
    _id,
    name,
    logo,
    postCode,
    phoneNo,
    coupon,
    capacity,
    uniId,
    discount,
    table,
    serviceCharge,
    address: {address, latitude, longitude},
    restaurantTiming: {
      openingTimeMonday,
      closingTimeMonday,
      deliveryTimes,
      collectionTime,
      bookingStartTime,
      bookingEndTime,
    },
    restaurantDelivery: {
      isDeliveryAvailable,
      isRiderService,
      areaLimit,
      deliveryCharge,
    },
  } = data;

  dispatch({
    type: SET_RESTAURANT_DATA,
    payload: {
      _id,
      name,
      logo,
      postCode,
      phoneNo,
      address,
      openingTimeMonday,
      closingTimeMonday,
      coupon,
      capacity,
      latitude,
      longitude,
      uniId,
      deliveryTimes,
      collectionTime,
      bookingStartTime,
      bookingEndTime,
      isDeliveryAvailable,
      isRiderService,
      areaLimit,
      deliveryCharge,
      discount,
      table,
      serviceCharge,
    },
  });
};

export const updateRestaurantAddress = async (id, payload, dispatch) => {
  try {
    const response = await axios.put(
      `${apiBaseUrl}address/update-restaurant-address/${id}`,
      payload,
    );

    return !!(response.data && response.data.success);
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return false;
  }
};

export const getRestaurantCategoryById = async id => {
  try {
    const response = await axios.get(
      `${apiBaseUrl}category/get-restaurant-categories/${id}`,
    );

    if (response.data) return response.data.data;
    return [];
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return [];
  }
};

export const getRestaurantItemsByCategoryId = async id => {
  try {
    const response = await axios.get(
      `${apiBaseUrl}food/get-foods-by-category/${id}`,
    );

    if (response.data) return response.data.data;
    return [];
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return [];
  }
};

export const getCategories = payload => {
  return (dispatch, getState) => {
    dispatch({type: SET_ALL_CATEGORIES, payload: payload});
  };
};
