const data = {
    userInfo: null,
};

const reducer = (state = data, action) => {
    switch (action.type) {
        case "SET_USER":
            return {
                ...state,
                userInfo: action.payload,
            };
        default:
            return state;
    }
};

export default reducer;


