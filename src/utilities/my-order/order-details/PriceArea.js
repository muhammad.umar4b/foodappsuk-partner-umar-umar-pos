import React from "react";
import {Text, View} from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function PriceArea({orderDetails}) {
    const {
        totalPriceArea,
    } = styles;

    const {
        flexDirectionRow,
        justifyBetween,
        fw700,
        paddingLeft5,
        paddingBottom1,
        paddingRight7,
        paddingTop1,
    } = globalStyles;

    const {
        subTotal,
        discount,
        orderType,
        serviceCharge,
        paymentMethod,
        deliveryCharge,
        totalPrice,
    } = orderDetails;

    return (
        <>
            <View style={paddingTop1}>
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Sub Total
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{subTotal ? parseFloat(subTotal).toFixed(2) : "0.00"}
                    </Text>
                </View>
            </View>

            {discount &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Discount (-)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{discount['discountValue'] ? (parseFloat(subTotal) * (parseFloat(discount['discountValue']) / 100)).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            <View style={[flexDirectionRow, justifyBetween]}>
                <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                    Service Charge (+)
                </Text>
                <Text style={[fw700, paddingRight7]}>
                    £{serviceCharge ? parseFloat(serviceCharge).toFixed(2) : "0.00"}
                </Text>
            </View>

            {orderType === "Home Delivery" &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Delivery Charge (+)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{deliveryCharge ? parseFloat(deliveryCharge).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            <View style={totalPriceArea}>
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Total Payment ({paymentMethod})
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{totalPrice ? parseFloat(totalPrice).toFixed(2) : "0.00"}
                    </Text>
                </View>
            </View>
        </>
    );
}
