import React from "react";
import {Text, View} from "react-native";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

export default function Footer({cartList}) {
    const {
        deliveryArea,
    } = styles;

    const {
        f18,
        fw700,
        textCenter,
    } = globalStyles;

    const {
        note,
    } = cartList;

    return (
        <View style={deliveryArea}>
            <Text style={[fw700, textCenter, f18]}>
                Kitchen Notes: {note}</Text>
        </View>
    );
}
