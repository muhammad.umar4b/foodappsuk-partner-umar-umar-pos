import React, {useEffect, useState} from "react";
import {Text, View} from "react-native";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

import {getTotalPrice} from "../../screens/in-house/payment/utility";

export default function PriceArea({cartList, restaurantData, service_type, deliveryCharge}) {
    const [totalPrice, setTotalPrice] = useState(0);

    const {
        subTotal,
    } = cartList;

    useEffect(() => {
        if (restaurantData) {
            const {
                serviceCharge
            } = restaurantData;

            const updateTotalPrice = getTotalPrice(service_type, subTotal, serviceCharge, deliveryCharge);
            setTotalPrice(updateTotalPrice);
        }
    }, [cartList, deliveryCharge]);

    const {
        totalPriceArea,
    } = styles;

    const {
        flexDirectionRow,
        justifyBetween,
        fw700,
        paddingLeft5,
        paddingBottom1,
        paddingRight7,
        paddingTop1,
    } = globalStyles;

    const {
        serviceCharge
    } = restaurantData;

    return (
        <>
            <View style={paddingTop1}>
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Sub Total
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{subTotal ? parseFloat(subTotal).toFixed(2) : "0.00"}
                    </Text>
                </View>
            </View>

            {service_type === "dine_in" &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Service Charge (+)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{serviceCharge ? parseFloat(serviceCharge).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            {service_type === "delivery" &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Delivery Charge (+)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{deliveryCharge ? parseFloat(deliveryCharge).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            <View style={totalPriceArea}>
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Total Price
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{totalPrice ? totalPrice.toFixed(2) : "0.00"}
                    </Text>
                </View>
            </View>
        </>
    );
}
